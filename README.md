# Production mode

`docker-compose up -d --build`

# Development mode

`docker-compose -f dc.dev.yml up -d --build`