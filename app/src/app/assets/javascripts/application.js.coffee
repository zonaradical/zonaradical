# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
# about supported directives.

#= require jquery
#= require jquery-ui
#= require jquery_ujs
#= require_tree ./shared
#= require select2
#= require select2_locale_pt-BR

# Запускаем когда страница готова | trigger when page is ready
$(document).ready ->
  # Main filter autocomplete
  $('#_tag_list').autocomplete
    minLength: 2
    source: '/tags/autocomplete'

  #Scroll to-top
  $("a.to-top").click ->
    $("html, body").animate
      scrollTop: 0
    , "slow"
    return

  $("a[href=#]").click (e) ->
    e.preventDefault()
    return

  $("#signIn").dialog
    autoOpen: false
    modal: true

  $("span.signIn-dialog").click ->
    $("#signIn").dialog "open"
    $('#new_comment').data('submitted', false)
    return

  $('#new_comment').on 'submit', ->
    if !$(this).data('logged-user')
      $(this).data('submitted', true)
      $("#signIn").dialog "open"
      return false

    return

  $("form#sign_in_user, form#sign_up_user").bind("ajax:success", (event, xhr, settings) ->
    if $('#new_comment').data('submitted')
      $('input[name=authenticity_token]').val(xhr.auth_token)
      $('#new_comment').data('logged-user', true).submit();
    else
      window.location.reload()
    $( "#signIn" ).dialog( "close" );
  ).bind("ajax:error", (event, xhr, settings, exceptions) ->
    error_messages = if xhr.responseJSON['error']
      "<div class='alert alert-danger pull-left'>" + xhr.responseJSON['error'] + "</div>"
    else if xhr.responseJSON['errors']
      $.map(xhr.responseJSON["errors"], (v, k) ->
        "<div class='alert alert-danger pull-left'>" + k + " " + v + "</div>"
      ).join ""
    else
      "<div class='alert alert-danger pull-left'>Unknown error</div>"
    $(this).parents('#signIn').children('.modal-footer').html(error_messages)
  )

  $('.related--slider').slick
    arrows: true,
    dots: false,
    infinite: false,
    variableWidth: true


  return


# Другие события | optional triggers
#
# $(window).load(function() { // Когда страница полностью загружена
#
# });
#
# $(window).resize(function() { // Когда изменили размеры окна браузера
#
# });
#
#
$(".flash").fadeIn ->
  secondsToFade = $(this).data('seconds-to-fade') || 5
  _that = this
  setTimeout (->
    $(_that).fadeOut()
    return
  ), (secondsToFade * 1000)
  return

((d, s, id) ->
  js = undefined
  fjs = d.getElementsByTagName(s)[0]
  if d.getElementById(id)
    return
  js = d.createElement(s)
  js.id = id
  js.src = '//connect.facebook.net/en_US/all.js#xfbml=1'
  fjs.parentNode.insertBefore js, fjs
  return
) document, 'script', 'facebook-jssdk'

do ->
  s = document.createElement('script')
  s.type = 'text/javascript'
  s.async = true
  s.src = (if 'https:' == document.location.protocol then 'https://s' else 'http://i') + '.po.st/static/v4/post-widget.js#publisherKey=g9v1ifbt8labgq1ss07n'
  x = document.getElementsByTagName('script')[0]
  x.parentNode.insertBefore s, x
  return
