# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require fancybox
#= require_tree ./editor
#=require sliders/slick
#=require shared/flipclock

$(document).ready ->
  $('.toggle-image').click ->
    button = $(this)
    id = button.data('image-id')
    if id != ''
      $.ajax
        url: '/contests/toggle_instagram_image/'+id
        dataType: 'json'
        async: false
        success: (data) ->
          if button.html() == 'publish'
            button.html('unpublish')
          else
            button.html('publish')
    return

  $('a.fancybox').fancybox
    helpers:
      title: type: 'inside'
  image_id = countdown = $('#preview_image').val()
  if image_id != undefined
    $('#instagram_image_' + image_id).trigger('click')

  $(".froala").froala_editor()

  # .index-slider Slick
  $(".contest-slider").slick
    dots: false
    arrows: true
    infinite: true
    speed: 500
    autoplay: false

  $("#contest_title").slug_preview({ slug_selector: '#contest_slug' })
  countdown = $('#countdown').val()
  $('.countdown').FlipClock(countdown,
    clockFace: 'DailyCounter'
    countdown: true,
    showSeconds: false)

  return
