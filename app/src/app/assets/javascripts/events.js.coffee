#= require_tree ./editor
#= require sliders/slick

$ ->
  resorts = $('#event_resort_ids').html()

  populate_resorts = ->
    options = []
    for country in $('#event_resort_category_ids').select2('data')
      options.push($(resorts).filter("optgroup[label='#{country.text}']"))
    $('#event_resort_ids').html(options)

  $('select[multiple=multiple]').select2()

  $('#event_resort_category_ids').on 'change', ->
    $('#event_resort_ids').select2('val', 'All')
    populate_resorts()

  populate_resorts()

  $(".froala").froala_editor()

  $("#event_title").slug_preview({ slug_selector: '#event_slug' })

  $('#event_start_date, #event_end_date').datepicker
    dateFormat: 'dd/mm/yy'

  # .index-slider Slick
  $(".tip-slider").slick
    dots: false
    arrows: true
    infinite: true
    speed: 500
    autoplay: false
