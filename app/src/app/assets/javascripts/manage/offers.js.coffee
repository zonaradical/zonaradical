#= require_tree ./../editor
#= require sliders/slick

$ ->
  resorts = $('#offer_resort_ids').html()

  populate_resorts = ->
    options = []
    resorts_displayed = []
    for country in $('#offer_resort_category_ids').select2('data')
      if country.text not in resorts_displayed
        options.push($(resorts).filter("optgroup[label='#{country.text}']"))
        resorts_displayed.push(country.text)

      $.ajax
        url: "/resort_categories/#{country.id}/children"
        success: (data) ->
          for child in data
            if child not in resorts_displayed
              options.push($(resorts).filter("optgroup[label='#{child}']"))

          $('#offer_resort_ids').html(options)

    if options.length == 0
      $('#offer_resort_ids').html(options)

  $('select[multiple=multiple]').select2()

  $('#offer_resort_category_ids').on 'change', ->
    $('#offer_resort_ids').select2('val', 'All')
    populate_resorts()

  populate_resorts()

  # .index-slider Slick
  $(".tip-slider").slick
    dots: false
    arrows: true
    infinite: true
    speed: 500
    autoplay: false

  $(".froala").froala_editor()

  $('#show_passed').on 'change', ->
    $('#show_passed_form').submit()

  tabIndex = $('.tab-panel').index($('.activate'))
  $('#tabs').tabs
    active: tabIndex
    activate: (event, ui) ->
      $('#offer_kind').val(ui.newPanel.attr('data-offer-kind'))
      return
  $('#offer_active_from, #offer_active_till').datepicker
    dateFormat: 'dd/mm/yy'

  $("#offer_title").slug_preview({ slug_selector: '#offer_slug' })
