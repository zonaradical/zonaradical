#= require_tree ./../editor
#= require sliders/slick

$ ->
  resorts = $('#tour_resort_ids').html()

  populate_resorts = ->
    options = []
    resorts_displayed = []
    for country in $('#tour_resort_category_ids').select2('data')
      if country.text not in resorts_displayed
        options.push($(resorts).filter("optgroup[label='#{country.text}']"))
        resorts_displayed.push(country.text)

      $.ajax
        url: "/resort_categories/#{country.id}/children"
        success: (data) ->
          for child in data
            if child not in resorts_displayed
              options.push($(resorts).filter("optgroup[label='#{child}']"))
          $('#tour_resort_ids').html(options)

    if options.length == 0
      $('#tour_resort_ids').html(options)

  $('select[multiple=multiple]').select2()

  $('#tour_resort_category_ids').on 'change', ->
    $('#tour_resort_ids').select2('val', 'All')
    populate_resorts()

  populate_resorts()

  # .index-slider Slick
  $(".tip-slider").slick
    dots: false
    arrows: true
    infinite: true
    speed: 500
    autoplay: false

  $(".froala").froala_editor()

  $("#shareTour").dialog
    autoOpen: true
    modal: true

  $('#show_passed').on 'change', ->
    $('#show_passed_form').submit()

  $("#tour_title").slug_preview({ slug_selector: '#tour_slug' })
