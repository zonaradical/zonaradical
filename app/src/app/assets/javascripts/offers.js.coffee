#= require_tree ./editor
#= require sliders/slick
#= require search.helpers
#= require jquery.scrollTo
#= require markdown.converter
#= require markdown.sanitizer
#= require markdown.editor

$ ->
  # .index-slider Slick
  $(".tip-slider").slick
    dots: false
    arrows: true
    infinite: true
    speed: 500
    autoplay: false

  configure_search_elements()

  converter = Markdown.getSanitizingConverter()
  editor = new Markdown.Editor(converter)
  editor.run()
