$.fn.extend
  slug_preview: (options) ->
    original_value = $(this).val()
    $(this).focusout ->
      element = $(this)
      value = element.val()
      if original_value != value
        slug_selector = options.slug_selector

        if (value != '')
          $.ajax
           url: element.data('url')
           data: { title: value }
           success: (data) ->
             $(slug_selector).val(data.slug)
        else
          $(slug_selector).val('')
