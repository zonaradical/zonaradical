#= require tag-it.min

$('.tag_list input').tagit
  allowSpaces: true
  placeholderText: 'Separated by commas',
  tagSource: '/tags/autocomplete'
