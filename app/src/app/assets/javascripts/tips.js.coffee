# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require_tree ./editor
#=require sliders/slick
#= require markdown.converter
#= require markdown.sanitizer
#= require markdown.editor

$ ->
  set_froala()

  $("#tabs").tabs()

  initialSlide = parseInt(window.location.hash.substr(1)) || 0

  # .index-slider Slick
  $(".tip-slider").slick
    dots: true
    arrows: true
    infinite: true
    speed: 500
    autoplay: false
    initialSlide: initialSlide
    adaptiveHeight: true

  $('.tip-slider').on 'afterChange', (event, slick, currentSlide) ->
    window.location.hash = currentSlide

  $('.extra_arrows button.prev_arrow').on 'click', () ->
    $('.tip-slider').slick('slickPrev');

  $('.extra_arrows button.next_arrow').on 'click', () ->
    $('.tip-slider').slick('slickNext');

  $("#tip_title").slug_preview({ slug_selector: '#tip_slug' })

  $('.type_description').on 'change', () ->
    value = $(this).val()
    textarea = $('textarea[id^="gallery_images_description"], textarea[id^="new_slide__description"]')
    if value == 'text_below' 
      textarea.addClass('froala')
      set_froala()
    else
      textarea.each (index) ->
        el = $(this)
        el.closest('div.gallery_desc').empty().html(el.show())
  
  $('#add-tip-slider').on ' click', () ->
    elem = $('#new-slider .item:first').clone()
    table = elem.find('table')
    description = elem.find('.gallery_desc')
    textarea = description.find('textarea')
    description.empty().append(textarea)
    elem.empty().append(table).append(description)
    $('#new-slider').append(elem)
    set_froala()

  converter = Markdown.getSanitizingConverter()
  editor = new Markdown.Editor(converter)
  editor.run()

  return

set_froala = () ->
  $(".froala").froala_editor()
