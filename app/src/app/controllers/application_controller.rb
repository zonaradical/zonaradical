class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  before_filter :set_last_seen_at, if: proc { |p| user_signed_in? && (session[:last_seen_at] == nil || session[:last_seen_at] < 15.minutes.ago) }
  before_filter :prepare_for_mobile
  before_filter :remind_email_confirmation

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale, :latest_blogs
  # include ApplicationHelper

  def latest_blogs
    @blogs=Blog.order(created_at: :desc)[0..4]
  end

  def set_locale
    # I18n.locale = params[:locale] || I18n.default_locale
    if Rails.env.development?
      I18n.locale = params[:locale] || session[:locale] || I18n.default_locale
    else
      I18n.locale = params[:locale] || locale_from_url || I18n.default_locale
    end
    session[:locale] = I18n.locale
  end

  # ToDo:  ?here

  def locale_from_url
    ori_url=URI(request.original_url).host.sub("www.",'')
    # ori_url=ori_url
    result=view_context.all_locale_webs.select{|key, valu| valu.include? ori_url }.keys[0].to_s
    result
  end
  helper_method :locale_from_url

  def ensure_signup_complete
    # Ensure we don't go into an infinite loop
    return if action_name == 'finish_signup'

    # Redirect to the 'finish_signup' page if the user
    # email hasn't been verified yet
    if current_user && !current_user.email_verified?
      redirect_to finish_signup_path(current_user)
    end
  end

  def access_denied(exception)
    redirect_to root_url, :alert => exception.message
  end

  def set_froala_config
    gon.aws_hash ||= AmazonSignature::data_hash
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  private

  def record_not_found
    render file: 'public/404.html', status: 404
    logger.warn "Route #{request.original_url} rise RecordNotFound"
  end

  def set_last_seen_at
    current_user.update_attribute(:last_seen_at, Time.now)
    session[:last_seen_at] = Time.now
  end

  def mobile_device?
    if session[:mobile_param]
      session[:mobile_param] == '1'
    else
      request.user_agent =~ /Mobile|webOS/
    end
  end

  helper_method :mobile_device?

  def prepare_for_mobile
    session[:mobile_param] = params[:mobile] if params[:mobile]
  end

  def remind_email_confirmation
    if user_signed_in? && !current_user.confirmed?
      flash[:warning] = t('pleaseConfirmEmail')
    end
  end

  def load_show_resource
    model_name     = controller_name.classify
    relation_model = relations.fetch(model_name, nil)

    resource = model_name.constantize.find(params[:id])

    model_obj = instance_variable_set("@#{controller_name.singularize}", resource) # set @resource

    if relation_model
      relation =
        model_name == 'Resort' ? 'country' : relation_model.to_s.underscore

      relation_id = params[:relation_id] || params[:category_id]
      record_not_found if model_obj.send(relation) != relation_model.find(relation_id)
    end
  end

  def relations
    @relations ||=  {
      'Tip'    => TipCategory,
      'Resort' => ResortCategory,
      'Video'  => VideoCategory
    }
  end
end
