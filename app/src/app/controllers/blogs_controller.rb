class BlogsController < ApplicationController
  include Gallerable
  before_action :set_froala_config
  before_action :set_blog, only: [:show, :edit, :update, :destroy]

  # respond_to :html

  def index
    @blogs = Blog.order(orig_date: :desc).page(params[:page])
    @authors=User
  end

  def show
  end

  def new
    @blog = Blog.new
  end

  def edit
  end

  def create
    @blog = Blog.new(blog_params)
    # @blog.user = current_user
    respond_to do |format|
      if @blog.save

        create_new_slide(@blog)

        format.html { redirect_to @blog, notice: 'blog was successfully created.' }
        format.json { render :show, status: :created, location: @blog }
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      create_new_slide(@blog)
      update_gallery_images(@blog)

      if @blog.update(blog_params)
        format.html { redirect_to @blog, notice: 'blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to blogs_url, notice: 'blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_blog
      @blog = Blog.find(params[:id])
    end

    def blog_params
      params.require(:blog).permit(:title,
                                   :image,
                                   :remove_image,
                                   :remove_img,
                                   :content,
                                   :tag_list,
                                   :user_id,
                                   :orig_date,
                                   :orig_url)
    end
end
