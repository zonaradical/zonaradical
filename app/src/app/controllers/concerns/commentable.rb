module Commentable
  module Controller
    extend ActiveSupport::Concern

    included do
      skip_authorize_resource only: :comment

      def comment
        authorize! :create, :comment
        build_comment
        save_comment or render 'show'
      end

      def build_comment
        @comment ||= Comment.new
        @comment.resource = comment_resource
        @comment.user_id = current_user.id
        @comment.reply_to = comment_params[:reply_to]
        @comment.content = comment_params[:content]
      end

      def save_comment
        redirect_to params['comment']['path'] if @comment.save
      end

      def comment_params
        comment_params = params[:comment]
        comment_params ? comment_params.permit(:content, :reply_to) : {}
      end

      def comment_resource
        instance_variable_get "@#{controller_name.classify.underscore.downcase}"
      end
    end
  end
end
