module Gallerable
  extend ActiveSupport::Concern

  def create_gallery_images(model)
    unless params[:gallery_images].nil? || params[:gallery_images][:images].nil?
      params[:gallery_images][:images].each do |i|
        model.gallery_images.create image: i
      end
    end
  end

  def create_new_slide(model)
    unless params[:new_slide].nil?
      params[:new_slide].each do |slide|
        if !slide[:description].blank? || !slide[:image].nil?
          model.gallery_images.create(name: slide[:name], image: slide[:image], url: slide[:url], order: slide[:order], description: slide[:description])
        end
      end
    end
  end

  def update_gallery_images(model)
    unless params[:gallery_images].nil?
      unless params[:gallery_images][:images].nil?
        params[:gallery_images][:images].each do |i|
          model.gallery_images.create image: i
        end
      end
      unless params[:gallery_images][:description].nil?
        params[:gallery_images][:description].each do |k, d|
          GalleryImage.find(k).update(description: d)
        end
      end
      unless params[:gallery_images][:order].nil?
        params[:gallery_images][:order].each do |k, d|
          GalleryImage.find(k).update(order: d)
        end
      end
      unless params[:gallery_images][:name].nil?
        params[:gallery_images][:name].each do |k, d|
          GalleryImage.find(k).update(name: d)
        end
      end
      unless params[:gallery_images][:url].nil?
        params[:gallery_images][:url].each do |k, d|
          GalleryImage.find(k).update(url: d)
        end
      end
      unless params[:gallery_images][:remove_img].nil?
        params[:gallery_images][:remove_img].each do |k, d|
          image = GalleryImage.find(k)
          image.remove_image!
          image.save
        end
      end
      unless params[:gallery_images][:remove_image].nil?
        params[:gallery_images][:remove_image].each do |k, d|
          GalleryImage.destroy(k)
        end
      end
      unless params[:gallery_images][:update_images].nil?
        params[:gallery_images][:update_images].each do |k, d|
          model.gallery_images.find(k).update image: d
        end
      end
      unless params[:gallery_images][:description_type].nil?
        model.gallery_images.each do |gallery_image|
          gallery_image.update(description_type: params[:gallery_images][:description_type])
        end
      end
    end
  end
end
