class ContestsController < ApplicationController
  include Gallerable
  load_and_authorize_resource

  before_action :set_froala_config

  def index
    @contests = Contest.all
  end

  def show
    load_contest
    if @contest.start_date > DateTime.now && @contest.end_date > DateTime.now
      load_instagram_images
      render 'before_start'
    elsif @contest.start_date <= DateTime.now && @contest.  end_date > DateTime.now
      load_instagram_images
      render 'started'
    else
      load_instagram_image_leaders
      render 'after_finish'
    end
  end

  def preview_instagram_image

    @preview_instagram_image = InstagramImage.find_by_id params[:image_id]
    load_contest
    if @contest.no_pagination
      load_instagram_images
      render 'preview_instagram_image_popup'
    else
      render 'preview_instagram_image_simple'
    end
  end

  def new
    @contest = Contest.new
  end

  def edit
    load_contest_for_edit
    load_instagram_images_for_edit
  end

  def toggle_instagram_image
    @instagram_image = InstagramImage.find params[:image_id]
    @instagram_image.published = !@instagram_image.published
    @instagram_image.save
    render json: {}
  end

  def create
    @contest = Contest.new(contest_params)
    respond_to do |format|
      if @contest.save

        create_gallery_images(@contest)

        format.html { redirect_to show_contest_path(@contest,@contest.title), notice: 'Contest was successfully created.' }
        format.json { render :show, status: :created, location: @contest }
      else
        format.html { render :new }
        format.json { render json: @contest.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|

      update_gallery_images(@contest)

      if @contest.update(contest_params)
        format.html { redirect_to show_contest_path(@contest), notice: 'Tip was successfully updated.' }
        format.json { render :show, status: :ok, location: @contest }
      else
        format.html { render :edit }
        format.json { render json: @contest.errors, status: :unprocessable_entity }
      end
    end
  end

  def slug
    @contest = Contest.new title: params[:title]
    render json: { slug: @contest.slug_preview }
  end

  private

  def load_instagram_images
    @hashtag = @contest.hashtag
    if @contest.no_pagination
      @instagram_images = InstagramImage.published.ordered.where(hashtag: @hashtag)
    else
      @instagram_images = InstagramImage.published.ordered.where(hashtag: @hashtag).paginate(:page => params[:page])
    end
  end

  def load_instagram_image_leaders
    @hashtag = @contest.hashtag
    @instagram_images = InstagramImage.published.where(hashtag: @hashtag).order('fb_likes desc').limit(3)
  end

  def load_instagram_images_for_edit
    @hashtag = @contest.hashtag
    @instagram_images = InstagramImage.where(hashtag: @hashtag).paginate(:page => params[:page])
  end


  def load_contest
    params[:title] ||= 'MaisEstiloZR'
    @contest = Contest.find(params[:title])
  end

  def load_contest_for_edit
    params[:id] ||= 'MaisEstiloZR'
    @contest = Contest.find(params[:id])
  end

  def contest_params
    params.require(:contest).permit(:id,:title, :hashtag, :rules, :no_pagination,
                                    :image, :remove_image,
                                    :start_date_d,:start_date_m,:start_date_y,
                                    :end_date_d,:end_date_m,:end_date_y,
                                    :description, :slug)
  end
end
