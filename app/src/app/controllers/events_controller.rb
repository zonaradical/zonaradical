class EventsController < ApplicationController
  include Gallerable
  load_and_authorize_resource

  before_action :set_froala_config

  def index
    @events = Event.page(params[:page]).most_recent.in_progress
  end

  def show
  end

  def new
    @event = Event.new
  end

  def edit; end

  def create
    @event = Event.new(event_params)
    if @event.save
      create_gallery_images(@event)
      redirect_to events_path, notice: 'Event was successfully created.'
    else
      render 'new'
    end
  end

  def update
    if @event.update_attributes(event_params)
      update_gallery_images(@event)
      redirect_to events_path, notice: 'Event was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to events_path, notice: 'Event was successfully destroyed.'
  end

  def slug
    @event = Event.new title: params[:title]
    render json: { slug: @event.slug_preview }
  end

  private

  def event_params
    params.require(:event).permit(
      :title,
      :slug,
      :tag_list,
      :image,
      :remove_image,
      :start_date,
      :end_date,
      :short_description,
      :description,
      resort_category_ids: [],
      resort_ids: []
    )
  end
end
