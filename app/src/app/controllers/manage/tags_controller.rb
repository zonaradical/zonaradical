class Manage::TagsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def index
    @tags = Tag.all
  end

  def new
    @tag = Tag.new
  end

  def create
    @tag = Tag.new(tag_params)
    if @tag.save
      redirect_to manage_tags_path, notice: 'Tag was successfully created.'
    else
      render 'new'
    end
  end

  def update
    if @tag.update_attributes(tag_params)
      redirect_to manage_tags_path, notice: 'Tag wass successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    @tag.delete
    redirect_to manage_tags_path, notice: 'Tag was successfully destroyed.'
  end

  private

  def tag_params
    params.require(:tag).permit(:name)
  end
end
