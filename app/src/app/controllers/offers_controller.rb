class OffersController < ApplicationController
  before_action :set_froala_config

  def index
    load_offers
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    load_offer
  end

  def search
    load_filtered_offers
    respond_to do |format|
      format.js
    end
  end

  def offers
    load_offers
  end

  def agencies
    def total_offers(user)
      Offer.published.switched_on.owned_by([user]).count
    end
    @users = User.agencies.sort_by { |a| [-total_offers(a), a.surname.to_s.downcase ] }
  end

  private

  def load_offers
    offers = offer_scope
    offers = offers.by_country_or_resort(params[:slug]) if params[:slug]
    @offers ||= offers.switched_on.order_by_start_date.paginate(page: params[:page]).to_a
  end

  def load_offer
    @offer ||= offer_scope.find(params[:id])
    if params[:resort_country_id].present? && !offer_scope.by_country_or_resort(params[:resort_country_id]).where(id: @offer).exists?
      record_not_found
    else
    end
  end

  def load_filtered_offers
    filter_options = search_params.delete_if { |k,v| v == "" }

    if filter_options['period']
      check_in_m, check_in_y = filter_options['period'].split('/')
      filter_options['check_in_m'] = check_in_m.to_i
      filter_options['check_in_y'] = check_in_y.to_i
      filter_options['active_month'] = Date.new(check_in_y.to_i, check_in_m.to_i)
      filter_options.delete('period')
    end

    if filter_options['cost']
      cost = filter_options['cost']
      cost = Range.new(*cost.split('..').map { |n| BigDecimal(n).to_f })
      filter_options['cost'] = cost
    end

    if filter_options['age_group']
      age_group = filter_options['age_group']
      age_group = Range.new(*age_group.split('..').map { |d| Date.parse(d) })
      filter_options['age_group'] = age_group
    end

    @offers ||= offer_scope.switched_on.filter(filter_options).order_by_start_date.paginate(page: params[:page]).to_a
  end

  def search_params
    search_params = params[:search]
    search_params ? search_params.permit(:resort_categories, :period, :tour_style, :accommodation, :cost, :age_group, :resorts, :show_passed) : {}
  end

  def offer_scope
    Offer.published.order_by_start_date
  end
end
