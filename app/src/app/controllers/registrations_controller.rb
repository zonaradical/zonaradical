class RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters

  respond_to :html, :json

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[name surname])
  end
end
