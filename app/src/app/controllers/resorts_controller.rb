class ResortsController < ApplicationController
  authorize_resource
  load_resource expect: :show

  skip_authorize_resource only: :resort_redirect

  before_action :load_show_resource, only: :show
  before_action :set_froala_config

  # GET /resorts
  # GET /resorts.json
  def index
    @all_resorts = Resort.all.sort_by{|a| a.name }.paginate(:page => params[:page], :per_page => 5)
  end

  # GET /resorts/1
  # GET /resorts/1.json
  def show
  end

  # GET /resorts/new
  def new
    @resort = Resort.new
  end

  # GET /resorts/1/edit
  def edit
  end

  def slug
    resort = Resort.new name: params[:title]
    render json: { slug: resort.slug_preview }
  end

  def resort_redirect
    redirect_to show_resort_path(@resort.country, @resort), status: :moved_permanently
  end

  # POST /resorts
  # POST /resorts.json
  def create
    @resort = Resort.new(resort_params)
    respond_to do |format|
      if @resort.save
        unless params[:gallery_images].nil? || params[:gallery_images][:images].nil?
          params[:gallery_images][:images].each do |i|
            @resort.gallery_images.create image: i
          end
        end

        format.html { redirect_to show_resort_path(@resort.country, @resort), notice: 'Resort was successfully created.' }
        format.json { render :show, status: :created, location: @resort }
      else
        format.html { render :new }
        format.json { render json: @resort.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /resorts/1
  # PATCH/PUT /resorts/1.json
  def update
    respond_to do |format|
      if @resort.update(resort_params)

        unless params[:gallery_images].nil?
          unless params[:gallery_images][:images].nil?
            params[:gallery_images][:images].each do |i|
              @resort.gallery_images.create image: i
            end
          end
          unless params[:gallery_images][:description].nil?
            params[:gallery_images][:description].each do |k, d|
              GalleryImage.find(k).update(description: d)
            end
          end
          unless params[:gallery_images][:remove_image].nil?
            params[:gallery_images][:remove_image].each do |k, d|
              GalleryImage.destroy(k)
            end
          end
        end

        format.html { redirect_to show_resort_path(@resort.country, @resort), notice: 'Resort was successfully updated.' }
        format.json { render :show, status: :ok, location: @resort }
      else
        format.html { render :edit }
        format.json { render json: @resort.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /resorts/1
  # DELETE /resorts/1.json
  def destroy
    @resort.destroy
    respond_to do |format|
      format.html { redirect_to resorts_url, notice: 'Resort was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def resort_params
      params.require(:resort).permit(
        :name, :slug, :snow_forecast_id,
        :image, :remove_image, :resort_category_id,
        :web, :fb, :map_url, :level1_description,
        :level2_description,:level3_description,
        :airport, :altitude_top, :altitude_bottom,
        :drop, :terrain, :lifts, :slopes, :tag_list
      )
    end
end
