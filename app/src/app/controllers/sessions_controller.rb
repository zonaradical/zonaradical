class SessionsController < Devise::SessionsController
  respond_to :html, :json

  # override devise action
  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)
    yield resource if block_given?
    respond_with({resource: resource, auth_token: form_authenticity_token}, location: after_sign_in_path_for(resource))
  end
end
