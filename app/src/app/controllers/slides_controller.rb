class SlidesController < ApplicationController

  authorize_resource

  def index
    @slides = Slide.page(params[:page])
  end

  def show
    @slide = Slide.find(params[:id])
  end

  def new
    @slide = Slide.new
  end

  # GET /slides/1/edit
  def edit
    @slide = Slide.find(params[:id])
  end

  def create
    @slide = Slide.new(slide_params)
    respond_to do |format|
      if @slide.save
        format.html { redirect_to @slide, notice: 'Slide was successfully created.' }
        format.json { render :show, status: :created, location: @slide }
      else
        format.html { render :new }
        format.json { render json: @slide.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @slide = Slide.find(params[:id])
    respond_to do |format|
      if @slide.update(slide_params)
        format.html { redirect_to @slide, notice: 'Slide was successfully updated.' }
        format.json { render :show, status: :ok, location: @slide }
      else
        format.html { render :edit }
        format.json { render json: @slide.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @slide.destroy
    respond_to do |format|
      format.html { redirect_to slides_url, notice: 'slide was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def slide_params
    params.require(:slide).permit(
      :title,
      :description,
      :link,
      :image,
      :remove_image,
      :status
    )
  end

end
