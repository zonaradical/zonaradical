class SsearchController < ApplicationController
  def index
    if params[:ssearch]
      @resources = {
        tips: TipPresenter.wrap(Tip.search(params[:ssearch], session[:locale])),
        #resorts: ResortPresenter.wrap(Resort.tagged_with(params[:tag_list], any: true)),
        #videos: VideoPresenter.wrap(Video.tagged_with(params[:tag_list], any: true)),
        #galleries: ImageGalleryPresenter.wrap(ImageGallery.tagged_with(params[:tag_list], any: true)),
        #events: EventPresenter.wrap(Event.tagged_with(params[:tag_list], any: true)),
        #offers: OfferPresenter.wrap(Offer.tagged_with(params[:tag_list], any: true)),
        #breezes: BreezePresenter.wrap(Breeze.tagged_with(params[:tag_list], any: true)),
      }
    end
  end
end
