class StaticPagesController < ApplicationController
  def index
    # @todo Add caching of the data, for large tables
    # performance of the RANDOM order in DB is bad
    @resort = Resort.joins(:resort_category).where(resort_categories: { ancestry: '3' }).order('RANDOM()').first

    @blog =Blog.order(created_at: :desc).first
    @tip = Tip.order('RANDOM()').first
    @tipsOrdered = Tip.order(created_at: :desc)[0..2]
    @breezes = Breeze.order(created_at: :desc).limit(10)
    @gallery_image = GalleryImage.order(:created_at).last
    @trip = Tour.order(:created_at).last
    @slides = Slide.where(status: true).order('id DESC').limit(4)
    @event = Event.in_progress.order(:start_date).first
    # @instagram_images = InstagramImage.where(hashtag: 'MaisEstiloZR').order('fb_likes DESC').limit(3)

  end

  def galera
  end

  def friends
  end
end
