class TagsController < ApplicationController
  def index
    @resources = {
      tips: TipPresenter.wrap(Tip.tagged_with(params[:tag_list], any: true)),
      resorts: ResortPresenter.wrap(Resort.tagged_with(params[:tag_list], any: true)),
      videos: VideoPresenter.wrap(Video.tagged_with(params[:tag_list], any: true)),
      galleries: ImageGalleryPresenter.wrap(ImageGallery.tagged_with(params[:tag_list], any: true)),
      events: EventPresenter.wrap(Event.tagged_with(params[:tag_list], any: true)),
      offers: OfferPresenter.wrap(Offer.tagged_with(params[:tag_list], any: true)),
      breezes: BreezePresenter.wrap(Breeze.tagged_with(params[:tag_list], any: true)),
    }
  end

  def autocomplete
    term = "%#{params[:term]}%"
    render json: Tag.where('name LIKE ?', term).pluck(:name)
  end
end
