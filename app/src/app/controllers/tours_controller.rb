class ToursController < ApplicationController
  include ToursHelper

  include Commentable::Controller
  before_action :load_tour, only: :comment

  #load_resource expect: :show
  #before_action :load_show_resource, only: :show

  before_action :set_froala_config

  def index
    params.include?('search') ? load_filtered_tours :
    load_tours
    load_country_and_resort if params[:slug]
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    load_tour
  end

  def search
    load_filtered_tours
    respond_to do |format|
      format.js
    end
  end

  def offers
    load_offers
  end

  #private

  def load_tours
    tours = tour_scope
    tours = tours.by_country_or_resort(params[:slug]) if params[:slug]
    @tours ||= tours.switched_on.paginate(page: params[:page]).to_a
  end

  def load_offers
    @tours ||= tour_scope.owned_by(User.agencies).paginate(page: params[:page])
      .to_a
  end

  def load_tour
    @tour ||= tour_scope.find(params[:id])
    if params[:resort_country_id].present? && !tour_scope.by_country_or_resort(params[:resort_country_id]).where(id: @tour).exists?
      record_not_found
    else
    end
  end

  def load_filtered_tours
    filter_options = search_params.delete_if { |k,v| v == "" }

    if filter_options['period']
      check_in_m, check_in_y = filter_options['period'].split('/')
      filter_options['check_in_m'] = check_in_m.to_i
      filter_options['check_in_y'] = check_in_y.to_i
      filter_options.delete('period')
    end

    if filter_options['cost']
      cost = filter_options['cost']
      cost = Range.new(*cost.split('..').map { |n| BigDecimal(n).to_f })
      filter_options['cost'] = cost
    end

    if filter_options['age_group']
      age_group = filter_options['age_group']
      age_group = Range.new(*age_group.split('..').map { |d| Date.parse(d) })
      filter_options['age_group'] = age_group
    end

    @tours ||= tour_scope.filter(filter_options).paginate(page: params[:page]).to_a
  end

  def load_country_and_resort
    @resort = Tour.try_resort_by_slug(params[:slug])
    @country = Tour.try_country_by_slug(params[:slug])
  end

  def search_params
    search_params = params[:search]
    search_params ? search_params.permit(:resort_categories, :period, :tour_style, :accommodation, :cost, :age_group, :resorts, :show_passed) : {}
  end

  def tour_scope
    Tour.published.order(:check_in)
  end
end
