module ApplicationHelper

  def all_locale_webs
    #
    {:'pt-BR'=>'http://zonaradical.com.br', :en=>'http://zonaradical.org', :es=>'http://zonaradical.cl'}
    #.except!(:c)
  end

  def non_locale_webs
    all_locale_webs.except!(session[:locale].to_sym)
  end

  def nested_dropdown(items)
    result = []
    items.map do |item, sub_items|
      result << [('- ' * item.depth) + item.name, item.id]
      result += nested_dropdown(sub_items) unless sub_items.blank?
    end
    result
  end

  def sex sex
    if sex.blank?
      '?'
    elsif
      sex == 'm' ? t('man') : t('women')
    end
  end

  def des(user)
    if user.sex.downcase == 'm'
      'do'
    elsif user.sex.downcase == 'f'
      'da'
    else
      'de'
    end
  end

  def cut_phrase(origin,dim)
    if origin.length < dim
      out=origin
    else
      out=origin[0..dim]
      out+='...'
    end
  end

  def discourse_client
    @discourse_client ||= DiscourseZr.client
  end

  def tags_page?
    params[:controller] == 'tags' && params[:action] == 'index'
  end

  def ssearch_page?
    params[:controller] == 'ssearch' && params[:action] == 'index'
  end

  def similar_tips(reso)
    if reso.is_a?(Tip)
      all_tip=Tip.all.map{ |t| [t,(t.tag_list & reso.tag_list).size]}.sort_by {|a,n| n}.reverse[0..5]
      all_tip=all_tip.reject{|t,n| n<1 }.map{|t,n| t}
      all_tip.delete(reso)
      all_tip
    elsif reso.is_a?(Array)
      all_tip=Tip.all.map{ |t| [t,(t.tag_list & reso).size]}.sort_by {|a,n| n}.reverse[0..5]
      all_tip.reject{|t,n| n<1 }.map{|t,n| t}
    end
  end

end
