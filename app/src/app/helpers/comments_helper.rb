module CommentsHelper
  def comments_info(comments_count)
    pluralize(comments_count, t('comments'))
  end
end
