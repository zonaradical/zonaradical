module ContestsHelper


  def start_date_formatted(contest)
    if contest.start_date_d.nil?
      contest.start_date.strftime('%m/%Y')
    else
      contest.start_date.strftime('%d/%m/%Y')
    end
  end


  def contest_start_date_day(contest)
    contest.start_date_d
  end

  def contest_start_date_month(contest)
    if contest.start_date_m.nil?
      Time.now.month
    else
      contest.start_date_m
    end
  end

  def contest_start_date_year(contest)
    if contest.start_date_y.nil?
      Time.now.year
    else
      contest.start_date_y
    end
  end

  def end_date_formatted(contest)
    if contest.end_date_d.nil?
      contest.end_date.strftime('%m/%Y')
    else
      contest.end_date.strftime('%d/%m/%Y')
    end
  end


  def contest_end_date_day(contest)
    contest.end_date_d
  end

  def contest_end_date_month(contest)
    if contest.end_date_m.nil?
      Time.now.month
    else
      contest.end_date_m
    end
  end

  def contest_end_date_year(contest)
    if contest.end_date_y.nil?
      Time.now.year
    else
      contest.end_date_y
    end
  end

end
