module EventsHelper
  def event_period(event)
    "#{event.start_date.strftime('%d/%m/%Y')} - #{event.end_date.strftime('%d/%m/%Y')}"
  end

  def event_countries(event)
    event.resort_categories.collect(&:name).join(', ')
  end

  def event_resorts(event)
    event.resorts.collect(&:name).join(', ')
  end

  def event_resorts_link(event)
    @event.resorts.map do |resort|
      link_to resort.name, show_resort_path(resort.country, resort), target: '_blank'
    end.join(', ').html_safe
  end
end
