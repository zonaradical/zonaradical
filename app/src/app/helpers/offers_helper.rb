module OffersHelper
  def offer_participation_status(offer, options = {})
    participate_button = options[:participate_button] || false

    if offer.user_participants.include?(current_user)
      if offer.participants.where(user: current_user).first.approved?
        fa_icon('check-circle', text: t('youAreIn'), class: 'green')
      elsif offer.participants.where(user: current_user).first.refused?
        fa_icon('times-circle', text: t('participationRefused'), class: 'red')
      else
        fa_icon('question-circle', text: t('participationRequestSent'), class: 'yellow')
      end
    elsif participate_button
      form_for [offer, Offer::Participant.new] do |f|
        concat(f.hidden_field :offer_id, value: offer.id)
        concat(f.hidden_field :user_id, value: current_user.id)
        concat(f.submit t('participate'))
      end if current_user and not offer.user_owners.include?(current_user)
    end
  end

  def offer_resort_categories
    Offer.published.switched_on.collect(&:resort_categories).flatten.uniq.reject { |item| item.blank? }
  end

  def offer_dates
    months = []
    Offer.published.switched_on.each do |offer|
      if offer.kind_start_date?        
        months << [offer.check_in_y, offer.check_in_m]
        duration_till = offer.check_in + offer.duration.days
        months << [duration_till.year, duration_till.month] if duration_till.month != offer.check_in_m
      elsif offer.kind_active_interval?
        date_tuples([offer.active_from.year, offer.active_from.month], [offer.active_till.year, offer.active_till.month]).each { |m| months << m }
      end
    end
    months.collect {|m| Date.new(m[0],m[1])}.uniq.sort
  end

  def offer_styles
    Offer.published.switched_on.collect(&:tour_style).uniq
  end

  def offer_accommodations
    Offer.published.switched_on.collect(&:accommodation).uniq.reject { |item| item.blank? }
  end

  def offer_resorts
    Offer.published.switched_on.collect(&:resorts).flatten.uniq.reject { |item| item.blank? }
  end

  def check_in_formatted(offer)
    if offer.check_in_d.nil?
      offer.check_in.strftime('%m/%Y')
    else
      offer.check_in.strftime('%d/%m/%Y')
    end
  end

  def offer_countries(offer)
    offer.resort_categories.collect(&:name).join(', ')
  end

  def offer_resorts_names(offer)
    offer.resorts.collect(&:name).join(', ')
  end

  def offer_resorts_names_linked(offer)
    offer.resorts.collect do |resort|
      link_to resort.name, show_resort_path(resort.country, resort), target: '_blank'
    end.join(', ').html_safe
  end
end
