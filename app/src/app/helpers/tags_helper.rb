module TagsHelper
  def tag_list_linked(tag_list)
    return if tag_list.empty?
    linked_tags = tag_list.map { |t| link_to t, tags_path(tag_list: t) }.join(', ')
    "#{linked_tags}".html_safe
  end
end
