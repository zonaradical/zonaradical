# create_table :accommodations, force: true do |t|
#   t.string    :name
#   t.timestamp :created_at,  precision: 6
#   t.timestamp :updated_at,  precision: 6
#   t.text      :description
# end

class Accommodation < ActiveRecord::Base
  def to_s
    name
  end
end
