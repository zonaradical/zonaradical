# create_table :blogs, force: true do |t|
#   t.string   :title
#   t.text     :content
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.string   :image
#   t.integer  :user_id
#   t.datetime :orig_date
#   t.string   :orig_url
# end

class Blog < ActiveRecord::Base
  # include Slug
  # slugged :title

  # translates :title, :short_description, :level1_description, :level2_description, :level2_description

  delegate :url_helpers, to: 'Rails.application.routes'

  acts_as_taggable

  validates :title,
            :content, presence: true

  # validates :short_description, length: { maximum: 250 }

  # belongs_to :tip_category
  belongs_to :user
  # :author, class: User

  has_many :gallery_images, as: :gallerable

  mount_uploader :image, TipImageUploader
  def gallery_description_type
    gallery_images.first.try(:description_type)
  end
end
