# create_table :breezes, force: true do |t|
#   t.string    :title
#   t.text      :body
#   t.string    :url
#   t.string    :image
#   t.integer   :breeze_category_id
#   t.timestamp :created_at,         precision: 6
#   t.timestamp :updated_at,         precision: 6
# end

class Breeze < ActiveRecord::Base
  acts_as_taggable

  validates :title, presence: true
  validates :breeze_category_id, presence: true

  belongs_to :breeze_category
  mount_uploader :image, BreezeImageUploader
end
