require 'active_support/concern'

module Slug
  extend ActiveSupport::Concern

  module ClassMethods
    def slugged(field)
      extend FriendlyId
      friendly_id field, use: [:slugged, :finders]
    end
  end

  included do
    validates :slug, presence: true
    before_save :sanitize_slug

    def sanitize_slug
      self.slug = slug.gsub(/(\W)/, '-')
    end
  end

  def slug_preview
    set_slug
  end
end
