# create_table :contests, force: true do |t|
#   t.string    :title
#   t.string    :hashtag
#   t.text      :description
#   t.string    :image
#   t.string    :slug
#   t.date      :start_date
#   t.integer   :start_date_d
#   t.integer   :start_date_m
#   t.integer   :start_date_y
#   t.timestamp :created_at,    precision: 6
#   t.timestamp :updated_at,    precision: 6
#   t.text      :rules
#   t.date      :end_date
#   t.integer   :end_date_y
#   t.integer   :end_date_m
#   t.integer   :end_date_d
#   t.boolean   :no_pagination,               default: true
# end

class Contest < ActiveRecord::Base
  include Slug
  slugged :title

  validates :title, presence: true, uniqueness: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validate :start_cannot_be_after_end

  has_many :gallery_images, as: :gallerable

  mount_uploader :image, TipImageUploader

  before_validation do
    self.start_date = start_date_d.nil? ? Date.new(start_date_y, start_date_m) : Date.new(start_date_y, start_date_m, start_date_d)
    self.end_date = end_date_d.nil? ? Date.new(end_date_y, end_date_m) : Date.new(end_date_y, end_date_m, end_date_d)
  end

  def start_cannot_be_after_end
    if end_date < start_date
      errors.add(:start_date, "can't be after end date")
    end
  end
end
