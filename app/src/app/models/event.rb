# create_table :events, force: true do |t|
#   t.string    :title
#   t.string    :slug
#   t.text      :short_description
#   t.text      :description
#   t.timestamp :start_date,        precision: 6
#   t.timestamp :end_date,          precision: 6
#   t.timestamp :created_at,        precision: 6
#   t.timestamp :updated_at,        precision: 6
#   t.string    :image
# end
#
# add_index :events, [:end_date], name: :index_events_on_end_date, using: :btree
# add_index :events, [:start_date], name: :index_events_on_start_date, using: :btree
# add_index :events, [:title], name: :index_events_on_title, using: :btree

class Event < ActiveRecord::Base
  include Slug
  slugged :title
  acts_as_taggable

  has_many :gallery_images, as: :gallerable

  has_many :countries
  has_many :resort_categories, through: :countries

  has_many :event_resorts
  has_many :resorts, through: :event_resorts

  mount_uploader :image, TipImageUploader

  validates :title, :start_date, :end_date, presence: true
  validate :country_presence, :date_validation

  scope :most_recent, -> { order('start_date') }
  scope :in_progress, -> { where('end_date >= ?', Date.current) }

  private

  def country_presence
    errors.add(:base, :missing_country) if self.countries.empty?
  end

  def date_validation
    if start_date.present? && end_date.present?
      errors.add(:end_date, :invalid_end_date) if end_date < start_date
    end
  end
end
