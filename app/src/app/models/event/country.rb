# create_table :event_country_assignments, force: true do |t|
#   t.integer :event_id
#   t.integer :resort_category_id
# end
#
# add_index :event_country_assignments, [:event_id], name: :index_event_country_assignments_on_event_id, using: :btree
# add_index :event_country_assignments, [:resort_category_id], name: :index_event_country_assignments_on_resort_category_id, using: :btree

class Event::Country < ActiveRecord::Base
  self.table_name = 'event_country_assignments'

  belongs_to :event
  belongs_to :resort_category
end
