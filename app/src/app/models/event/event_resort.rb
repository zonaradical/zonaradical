# create_table :event_resort_assignments, force: true do |t|
#   t.integer :event_id
#   t.integer :resort_id
# end
#
# add_index :event_resort_assignments, [:event_id], name: :index_event_resort_assignments_on_event_id, using: :btree
# add_index :event_resort_assignments, [:resort_id], name: :index_event_resort_assignments_on_resort_id, using: :btree

class Event::EventResort < ActiveRecord::Base
  self.table_name = 'event_resort_assignments'

  belongs_to :event
  belongs_to :resort
end
