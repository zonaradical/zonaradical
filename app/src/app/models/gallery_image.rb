# create_table :gallery_images, force: true do |t|
#   t.integer   :resort_id
#   t.string    :image
#   t.text      :description
#   t.integer   :gallerable_id
#   t.string    :gallerable_type
#   t.timestamp :created_at,       precision: 6
#   t.timestamp :updated_at,       precision: 6
#   t.string    :name
#   t.string    :url
#   t.integer   :order
#   t.integer   :description_type,               default: 0
# end

class GalleryImage < ActiveRecord::Base
  enum description_type: [:text_covers, :text_below]

  belongs_to :gallerable, polymorphic: true
  mount_uploader :image, GalleryImageUploader

  default_scope {order :order}
end
