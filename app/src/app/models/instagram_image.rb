# create_table :instagram_images, force: true do |t|
#   t.string    :instagram_id
#   t.string    :hashtag
#   t.json      :data
#   t.timestamp :created_at,   precision: 6
#   t.timestamp :updated_at,   precision: 6
#   t.boolean   :published,                  default: true
#   t.integer   :fb_likes
# end
#
# add_index :instagram_images, [:instagram_id], name: :index_instagram_images_on_instagram_id, using: :btree

class InstagramImage < ActiveRecord::Base

  scope :published, -> { where(published: true) }
  scope :ordered, -> { order ('id ASC') }

  def username
    self.data['user']['username']
  end

  def full_name
    self.data['user']['full_name']
  end

  def thumb_url
    self.data['images']['thumbnail']['url']
  end

  def image_url
    self.data['images']['standard_resolution']['url']
  end

  def caption
    begin
      self.data['caption']['text']
    rescue  NoMethodError
      ''
    end
  end

  def user_image_url
    self.data['user']['profile_picture']
  end
end
