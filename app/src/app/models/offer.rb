# create_table :offers, force: true do |t|
#   t.integer   :tour_style_id
#   t.integer   :accommodation_id
#   t.string    :title
#   t.text      :description
#   t.string    :whats_included
#   t.integer   :duration
#   t.integer   :check_in_d
#   t.integer   :check_in_m
#   t.integer   :check_in_y
#   t.integer   :switch_off_d
#   t.integer   :switch_off_m
#   t.integer   :switch_off_y
#   t.date      :check_in
#   t.date      :switch_off
#   t.string    :image
#   t.decimal   :price,            precision: 5, scale: 0
#   t.boolean   :published,                                default: true
#   t.timestamp :created_at,       precision: 6
#   t.timestamp :updated_at,       precision: 6
#   t.string    :hotel_name
#   t.boolean   :air_included
#   t.string    :slug
#   t.date      :active_from
#   t.date      :active_till
#   t.integer   :kind
#   t.integer   :currency_id,                              default: 1
# end
#
# add_index :offers, [:accommodation_id], name: :index_offers_on_accommodation_id, using: :btree
# add_index :offers, [:tour_style_id], name: :index_offers_on_tour_style_id, using: :btree

class Offer < ActiveRecord::Base

  KIND_START_DATE = 1
  KIND_ACTIVE_INTERVAL= 2

  include Slug
  slugged :title
  acts_as_taggable

  belongs_to :tour_style
  belongs_to :accommodation
  belongs_to :currency
  has_many :owners
  has_many :user_owners, through: :owners, source: :user
  has_many :participants
  has_many :user_participants, through: :participants, source: :user
  has_many :countries
  has_many :resort_categories, through: :countries
  has_many :offer_resorts
  has_many :resorts, through: :offer_resorts
  has_many :gallery_images, as: :gallerable

  validates :tour_style, :title, :description, presence: true
  with_options if: :kind_start_date? do |start_date|
    start_date.validates :check_in_y, :check_in_m, presence: true
    start_date.validates :check_in_y, :check_in_m, numericality: { only_integer: true, greater_than: 0 }
  end
  with_options if: :kind_active_interval? do |active_interval|
    active_interval.validates :active_from,:active_till, presence: true
    active_interval.validate :active_from_cannot_be_after_active_till, unless: Proc.new { |a| a.active_from.nil? || a.active_till.nil? }
  end

  validate :country_presence
  validates :duration, presence: true


  mount_uploader :image, OfferImageUploader

  after_initialize :init

  before_save do
    if self.kind_start_date?
      self.switch_off_d = check_in_d
      self.switch_off_m = check_in_m
      self.switch_off_y = check_in_y

      self.check_in = check_in_d.nil? ? Date.new(check_in_y, check_in_m) : Date.new(check_in_y, check_in_m, check_in_d)
      self.switch_off = switch_off_d.nil? ? Date.new(switch_off_y, switch_off_m) : Date.new(switch_off_y, switch_off_m, switch_off_d)
      self.active_from = nil
      self.active_till = nil
    elsif self.kind_active_interval?
      self.check_in, self.check_in_y, self.check_in_m, self.check_in_d = nil
      self.switch_off, self.switch_off_y, self.switch_off_m, self.switch_off_d = nil
    end
  end

  scope :by_country, -> (country) { includes(:resort_categories).where(resort_categories: { id: country }) }
  scope :by_resort, -> (resort) { includes(:resorts).where(resorts: { id: resort }) }
  scope :order_by_start_date, -> { order('GREATEST(check_in,active_from) asc') }

  def approved_participants
    participants.where(status: Offer::Participant.statuses[:approved])
  end

  def owner
    user_owners.first
  end

  def self.by_country_or_resort(param)
    country = ResortCategory.find(param)
    by_country(country)
  rescue
    resort = Resort.find(param)
    by_resort(resort)
  end

  def self.published
    where(published: true)
  end

  def self.switched_on
    where("(switch_off >= :now AND kind=#{KIND_START_DATE}) OR (active_till >= :now AND kind = #{KIND_ACTIVE_INTERVAL})", {now: Date.today})
  end

  def self.involved(user)
    includes([:user_owners, :user_participants])
      .where("offer_user_assignments.user_id = :user_id
        OR offer_user_participant_assignments.user_id = :user_id",
        user_id: user.id)
      .references([:user_owners, :user_participants])
  end

  def self.owned_by(users)
    includes(:user_owners).where(
      offer_user_assignments: { user_id: users.collect(&:id) }
    )
  end

  def self.filter(options = {})
    offers = where(nil)
    offers = offers.includes(:resort_categories).where(resort_categories: { id: options['resort_categories'] }) if options['resort_categories']
    if options['active_month']
      offers = offers.where("active_from <= '#{options['active_month'].end_of_month}' AND active_till >= '#{options['active_month'].beginning_of_month}' OR (check_in <= '#{options['active_month'].end_of_month}' AND check_in + interval '1 day' * duration >= '#{options['active_month'].beginning_of_month}')")
    end
    offers = offers.where(tour_style: options['tour_style']) if options['tour_style']
    offers = offers.where(accommodation: options['accommodation']) if options['accommodation']
    offers = offers.where(price: options['cost']) if options['cost']
    if options['age_group']
      offers = offers.includes(:user_owners)
        .where(offer_user_assignments: { id: Tour::Owner.first_owners_by_tour })
        .where(users: { birthday: options['age_group'] })
    end
    offers = offers.includes(:resorts).where(resorts: { id: options['resorts'] }) if options['resorts']
    offers = offers.switched_on if options['show_passed'] != "1"
    offers
  end

  def kind_start_date?
    self.kind == KIND_START_DATE
  end
  def kind_active_interval?
    self.kind == KIND_ACTIVE_INTERVAL
  end

  private

  def self.use_relative_model_naming?
    true
  end

  def country_presence
    errors.add(:base, :missing_country) if self.countries.empty?
  end
  def init
    self.kind ||= KIND_START_DATE
  end
  def active_from_cannot_be_after_active_till
    if self.active_from > self.active_till
      errors.add(:active_from, "can't be after active till")
    end
  end
end
