# create_table :resorts, force: true do |t|
#   t.string    :name
#   t.string    :image
#   t.string    :web
#   t.string    :fb
#   t.integer   :resort_category_id
#   t.integer   :index
#   t.text      :level1_description
#   t.text      :level2_description
#   t.text      :level3_description
#   t.string    :airport
#   t.integer   :altitude_top
#   t.integer   :altitude_bottom
#   t.integer   :drop
#   t.integer   :terrain
#   t.integer   :lifts
#   t.string    :slopes
#   t.timestamp :created_at,         precision: 6
#   t.timestamp :updated_at,         precision: 6
#   t.string    :map_url
#   t.string    :slug
#   t.string    :snow_forecast_id
# end

class Resort < ActiveRecord::Base
  include Slug
  slugged :name
  acts_as_taggable

  validates :name, presence: true
  validates :resort_category_id, presence: true
  validates :fb, format: { with: URI.regexp }, allow_blank: true
  validates :web, format: { with: URI.regexp }, allow_blank: true
  validates :map_url, format: { with: URI.regexp }, allow_blank: true

  belongs_to :resort_category
  has_many :gallery_images, as: :gallerable

  mount_uploader :image, ResortImageUploader

  def country
    resort_category.ancestors.size > 1 ? resort_category.ancestors.second : resort_category
  end
end
