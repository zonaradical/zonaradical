# create_table :resort_categories, force: true do |t|
#   t.string  :name
#   t.text    :description
#   t.string  :ancestry
#   t.string  :index
#   t.integer :ancestry_depth,     default: 0
#   t.string  :image
#   t.string  :slug
#   t.integer :discourse_category
# end

class ResortCategory < ActiveRecord::Base
  ALLOWED_COUNTRIES_ID = [1, 3]

  include Slug
  slugged :name

  has_ancestry( :cache_depth => true )

  has_many :resorts
  validates :name, presence: true
  mount_uploader :image, CategoryImageUploader

  enum discourse_category: { other: 0, europe: 1 }

  def resorts_size_with_children
    resorts.size + children.inject(0) {|sum, child| sum + child.resorts_size_with_children }
  end

  def discourse_name
    ResortCategory.human_attribute_name discourse_category || '-', default: name.downcase
  end

  private

  def self.allowed_countries
    ResortCategory.where(id: ALLOWED_COUNTRIES_ID).map(&:descendants).flatten.sort_by { |country| country['name'] }
  end

  def self.allowed_countries_grouped
    result = []
    ResortCategory.roots.each do |root|
      res = []
      root.children.flatten.sort_by { |c| c[:name]}.each{ |i| res << [i.name, i.id]}
      result << [root.name, res]
    end
    result
  end
end
