# create_table :slides, force: true do |t|
#   t.string   :title
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.string   :description
#   t.string   :image
#   t.boolean  :status
#   t.string   :link
# end

class Slide < ActiveRecord::Base
	validates :title, presence: true
	mount_uploader :image, SlideImageUploader

	def slide_status
		if self.status == true
			return 'Ativo'
		else
			return 'Inativo'
		end
	end
end
