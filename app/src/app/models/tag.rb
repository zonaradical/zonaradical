# create_table :tags, force: true do |t|
#   t.string  :name
#   t.integer :taggings_count, default: 0
# end
#
# add_index :tags, [:name], name: :index_tags_on_name, unique: true, using: :btree

class Tag < ActsAsTaggableOn::Tag
  scope :used, -> { where('taggings_count > 0') }
end
