# create_table :tips, force: true do |t|
#   t.integer   :tip_category_id
#   t.string    :image
#   t.timestamp :created_at,         precision: 6
#   t.timestamp :updated_at,         precision: 6
#   t.text      :level3_description
#   t.string    :slug
#   t.integer   :discourse_topic_id
#   t.integer   :author_id
#   t.integer   :comments_count,                   default: 0
# end
#
# add_index :tips, [:author_id], name: :index_tips_on_author_id, using: :btree

class Tip < ActiveRecord::Base
  include Slug
  slugged :title

  translates :title, :short_description, :level1_description, :level2_description, :level2_description

  if Settings.enable_forum
    act_as_discoursable(
      title: -> (tip) {tip.title},
      description: -> (tip) {tip.short_description},
      category: -> (tip) {tip.tip_category.discourse_name},
      external_id: -> (tip) {tip.author.id}
    )
  end

  delegate :url_helpers, to: 'Rails.application.routes'

  acts_as_taggable

  validates :title,
            :tip_category_id,
            :short_description, presence: true

  validates :short_description, length: { maximum: 250 }

  belongs_to :tip_category
  belongs_to :author, class: User

  has_many :gallery_images, as: :gallerable

  mount_uploader :image, TipImageUploader

  def gallery_description_type
    gallery_images.first.try(:description_type)
  end

  def show_path
    url_helpers.show_tip_url(tip_category, self)
  end

  def self.search(search,loc)
    #where("title like ?", "%#{search}%")
    with_translations(loc).where("lower(title) like ?", "%#{search.downcase}%")
    with_translations(loc).where("lower(short_description) like ?", "%#{search.downcase}%")
    with_translations(loc).where("lower(level1_description) like ?", "%#{search.downcase}%")
  end

end
