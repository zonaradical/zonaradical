# create_table :tip_categories, force: true do |t|
#   t.string    :name
#   t.text      :description
#   t.string    :index
#   t.integer   :ancestry_depth,               default: 0
#   t.timestamp :created_at,     precision: 6
#   t.timestamp :updated_at,     precision: 6
#   t.string    :image
#   t.string    :slug
# end

class TipCategory < ActiveRecord::Base
  include Slug
  slugged :name

  has_many :tips
  validates :name, presence: true
  mount_uploader :image, CategoryImageUploader

  DISCOURSE_CATEGORIES = {
    'Dicionário'       => 'Aprendendo',
    'Snowboard et al.' => 'Aprendendo',
    'Viagens'          => 'Viagens',
    'Equipamento'      => 'Equipamento',
    'Eventos'          => 'Viagens'
  }

  def discourse_name
    DISCOURSE_CATEGORIES[name]
  end
end
