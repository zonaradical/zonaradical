# create_table :tour_styles, force: true do |t|
#   t.string    :name
#   t.string    :description
#   t.timestamp :created_at,  precision: 6
#   t.timestamp :updated_at,  precision: 6
# end

class TourStyle < ActiveRecord::Base
  def to_s
    name
  end
end
