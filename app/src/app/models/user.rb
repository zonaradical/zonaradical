# create_table :users, force: true do |t|
#   t.string    :email,                                default: "",               null: false
#   t.string    :encrypted_password,                   default: "",               null: false
#   t.string    :reset_password_token
#   t.timestamp :reset_password_sent_at, precision: 6
#   t.timestamp :remember_created_at,    precision: 6
#   t.integer   :sign_in_count,                        default: 0,                null: false
#   t.timestamp :current_sign_in_at,     precision: 6
#   t.timestamp :last_sign_in_at,        precision: 6
#   t.string    :current_sign_in_ip
#   t.string    :last_sign_in_ip
#   t.string    :confirmation_token
#   t.timestamp :confirmed_at,           precision: 6
#   t.timestamp :confirmation_sent_at,   precision: 6
#   t.string    :unconfirmed_email
#   t.timestamp :created_at,             precision: 6
#   t.timestamp :updated_at,             precision: 6
#   t.string    :name
#   t.string    :forem_state,                          default: :pending_review
#   t.boolean   :forem_auto_subscribe,                 default: false
#   t.timestamp :last_seen_at,           precision: 6
#   t.string    :avatar
#   t.string    :surname
#   t.string    :login
#   t.string    :sex
#   t.date      :birthday
#   t.string    :country
#   t.string    :city
#   t.string    :web
#   t.string    :fb
#   t.text      :bio
#   t.string    :fb_avatar
#   t.string    :image
#   t.string    :telephone
#   t.string    :slug
#   t.string    :small_logo
# end
#
# add_index :users, [:email], name: :index_users_on_email, unique: true, using: :btree
# add_index :users, [:reset_password_token], name: :index_users_on_reset_password_token, unique: true, using: :btree

class User < ActiveRecord::Base
  extend FriendlyId

  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/

  friendly_id :slug_candidates, use: [:slugged, :finders]

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  acts_as_messageable
  has_many :role_assignments
  has_many :roles, :through => :role_assignments
  has_many :image_galleries, as: :image_gallerable
  has_many :blogs
  #, as: :author
  has_many :tips, as: :author
  has_and_belongs_to_many :tours, join_table: 'tour_user_assignments'
  has_and_belongs_to_many :offers, join_table: 'offer_user_assignments'

  mount_uploader :avatar, AvatarImageUploader
  mount_uploader :small_logo, AvatarImageUploader
  mount_uploader :image, TipImageUploader

  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update
  validates_uniqueness_of :login, :allow_nil => true, :allow_blank => true

  validates :password, presence: true, length: {minimum: 5, maximum: 120}, on: :create
  validates :password, length: {minimum: 5, maximum: 120}, on: :update, allow_blank: true
  validates :name, :surname, presence: true
  validates :slug, uniqueness: true

  before_save :sanitize_url
  if Settings.enable_forum
    after_create :create_discourse_user
    after_save :update_discourse_user
  end


  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)
    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user
    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        sex = auth.extra.raw_info.gender == 'male' ? 'M' : 'F'
        surname = auth.info.name.split.drop(1).empty? ? auth.uid :  auth.info.name.split.drop(1).join(" ")
        name = auth.info.name.split[1].nil? ? auth.info.name :  auth.info.name.split[1]
        user = User.new(
          name: name,
          surname: surname,
          sex: sex,
          fb: auth.extra.raw_info.link,
          image: auth.info.image,
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.skip_confirmation!
        user.save!
      end
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def blogger?
    blogs.count > 0
  end


  def self.athletes
    joins(:roles).where(roles: { name: 'athlete' }).order(name: :asc)
  end

  def admin?
    roles.include?(Role.find_by_name(:admin))
  end

  def self.agencies
    includes(:roles).where(roles: { name: 'agency' })
  end

  def self.editors
    includes(:roles).where(roles: { name: 'editor' })
  end

  def self.organizations
    includes(:roles).where(roles: { name: 'organization' })
  end


  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

  def online?
    self.last_seen_at.to_i - self.current_sign_in_at.to_i > 0
  end

  def has_role?(role_sym)
    roles.any? { |r| r.name.underscore.to_sym == role_sym }
  end

  def admin?
    roles.include?(Role.find_by_name(:admin))
  end

  def agency?
    roles.include?(Role.find_by_name(:agency))
  end
  def athlete?
    roles.include?(Role.find_by_name(:athlete))
  end

  def org?
    roles.include?(Role.find_by_name(:organization))
  end

  def mailboxer_email(object)
    email
  end
  def younger_45?
    !self.birthday.blank? && self.age<=45
  end

  def age
    now = Time.now.utc.to_date
    now.year - self.birthday.year - ((now.month > self.birthday.month || (now.month == self.birthday.month && now.day >= self.birthday.day)) ? 0 : 1)
  end

  def notifications
    mailbox.notifications
  end

  def confirmed?
    self.unconfirmed_email.blank? && !!self.confirmed_at
  end

  private

  def create_discourse_user
    DiscourseZr.sync_sso(self)
  end

  def update_discourse_user
    sso_data = {
      sso_secret: Rails.application.secrets.discourse_secret,
      external_id: self.id
    }
    if self.name_changed? or self.email_changed?
      sso_data[:name] = self.name if self.name_changed?
      sso_data[:email] = self.email
      client = DiscourseZr.client
      client.sync_sso(sso_data)
    end
  end

  def slug_candidates
    [
      :login,
      [:name, :surname]
    ]
  end

  def should_generate_new_friendly_id?
    login_changed? || slug.blank?
  end

  def sanitize_url
    self.fb = "http://#{self.fb}" unless self.fb.to_s.empty? || self.fb =~ /^https?:\/\//
    self.web = "http://#{self.web}" unless self.web.to_s.empty? || self.web =~ /^https?:\/\//
  end
end
