# create_table :videos, force: true do |t|
#   t.string    :title
#   t.text      :description
#   t.integer   :source_cd
#   t.integer   :video_category_id
#   t.string    :source_link
#   t.timestamp :created_at,        precision: 6
#   t.timestamp :updated_at,        precision: 6
#   t.string    :slug
# end

class Video < ActiveRecord::Base
  include Slug
  slugged :title
  acts_as_taggable

  as_enum :source, youtube: 0, vimeo: 1
  belongs_to :video_category
  validates :title, presence: true
  validates :source_link, presence: true
end
