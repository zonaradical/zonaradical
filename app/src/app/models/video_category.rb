# create_table :video_categories, force: true do |t|
#   t.string    :name
#   t.text      :description
#   t.string    :ancestry
#   t.string    :index
#   t.integer   :ancestry_depth,               default: 0
#   t.timestamp :created_at,     precision: 6
#   t.timestamp :updated_at,     precision: 6
#   t.string    :image
#   t.string    :slug
# end

class VideoCategory < ActiveRecord::Base
  include Slug
  slugged :name

  has_ancestry( :cache_depth => true )
  has_many :videos

  validates :name, presence: true
  mount_uploader :image, CategoryImageUploader


  def videos_size_with_children
    videos.size + children.inject(0) {|sum, child| sum + child.videos_size_with_children }
  end


end
