class BreezePresenter < Burgundy::Item
  alias_attribute :summary, :body

  def show_url
    routes.breeze_path(self)
  end
end
