class CommentPresenter
  include ApplicationHelper

  def initialize(comment, all_comments=nil)
    @all_comments = all_comments
    @comment = comment
  end

  def user
    @user ||= User.find(discourse_client.user_sso(@comment['user_id'])['external_id'])
  end

  def user_avatar
    avatar_template = discourse_client.user_by_external_id(user.id)['avatar_template'].gsub('{size}', '45')
    "#{Rails.application.secrets.discourse_url}#{avatar_template}"
  end

  def title
    "#{user.name} #{user.surname}, #{DateTime.parse(@comment['updated_at']).strftime('%d de %B, %Y às %I:%M %p')}"
  end

  def content
    @comment['cooked'].html_safe
  end

  def replies
    @all_replies = []
    @all_comments.map do |co|
      if co['reply_to_post_number'] == @comment['post_number']
        inner_replies(co)
      end
    end
    @all_replies
  end

  def post_number
    @comment['post_number']
  end

  private

  def inner_replies(comment)
    @all_replies << comment
    @all_comments.map do |co|
      if co['reply_to_post_number'] == comment['post_number']
        inner_replies(co)
      end
    end
  end
end
