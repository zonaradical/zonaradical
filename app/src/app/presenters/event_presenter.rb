class EventPresenter < Burgundy::Item
  alias_attribute :summary, :short_description

  def show_url
    routes.event_path(self)
  end
end
