class ImageGalleryPresenter < Burgundy::Item
  alias_attribute :summary, :description

  def show_url
    routes.polymorphic_path([image_gallerable, self])
  end
end
