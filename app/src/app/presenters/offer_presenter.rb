class OfferPresenter < Burgundy::Item
  def show_url
    routes.show_offer_offers_path(helpers.country_or_resort(self), self)
  end

  def summary
    helpers.strip_tags description
  end
end
