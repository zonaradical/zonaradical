class ResortCategoryPresenter < Burgundy::Item
  alias_attribute :title, :name
  alias_attribute :summary, :description

  def show_url
    routes.show_resort_category_path(self)
  end
end
