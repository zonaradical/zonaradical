class ResortPresenter < Burgundy::Item
  alias_attribute :title, :name
  alias_attribute :summary, :level1_description

  def show_url
    routes.show_resort_path(resort_category, self)
  end
end
