class TipPresenter < Burgundy::Item
  alias_attribute :summary, :short_description

  def show_url
    routes.show_tip_path(tip_category, self)
  end
end
