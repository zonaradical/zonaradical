class VideoPresenter < Burgundy::Item
  alias_attribute :summary, :description

  def show_url
    routes.show_video_path(video_category, self)
  end
end
