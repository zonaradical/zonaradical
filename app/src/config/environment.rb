# Load the Rails application.
require File.expand_path('../application', __FILE__)

OmniAuth.config.full_host = ENV['FULL_HOST']

# Initialize the Rails application.
Rails.application.initialize!
