

# controllers

Rails.application.config.assets.precompile += %w( application_mobile.css )
Rails.application.config.assets.precompile += %w( aka_adap.css )
Rails.application.config.assets.precompile += %w( aka_adap/jquery-2.2.3.min.js )
Rails.application.config.assets.precompile += %w( aka_adap/slick.js )
Rails.application.config.assets.precompile += %w( aka_adap/scripts.js )
Rails.application.config.assets.precompile += %w( aka_adap/po-st.js )

Rails.application.config.assets.precompile += %w( static_pages.css )
Rails.application.config.assets.precompile += %w( static_pages.js )

Rails.application.config.assets.precompile += %w( users.css )
Rails.application.config.assets.precompile += %w( users.js )

Rails.application.config.assets.precompile += %w( blogs.css )
Rails.application.config.assets.precompile += %w( blogs.js )

Rails.application.config.assets.precompile += %w( resorts.css )
Rails.application.config.assets.precompile += %w( resorts.js )

Rails.application.config.assets.precompile += %w( resort_categories.css )
Rails.application.config.assets.precompile += %w( resort_categories.js )

Rails.application.config.assets.precompile += %w( tips.css )
Rails.application.config.assets.precompile += %w( tips.js )

Rails.application.config.assets.precompile += %w( tip_categories.css )
Rails.application.config.assets.precompile += %w( tip_categories.js )

Rails.application.config.assets.precompile += %w( image_galleries.css )
Rails.application.config.assets.precompile += %w( image_galleries.js )

Rails.application.config.assets.precompile += %w( videos.css )
Rails.application.config.assets.precompile += %w( videos.js )

Rails.application.config.assets.precompile += %w( video_categories.js )
Rails.application.config.assets.precompile += %w( video_categories.css )

Rails.application.config.assets.precompile += %w( tours.css )
Rails.application.config.assets.precompile += %w( tours.js )
Rails.application.config.assets.precompile += %w( markdown-buttons.png )

Rails.application.config.assets.precompile += %w( offers.css )
Rails.application.config.assets.precompile += %w( offers.js )

Rails.application.config.assets.precompile += %w( slides.css )
Rails.application.config.assets.precompile += %w( slides.js )

Rails.application.config.assets.precompile += %w( manage/tours.css )
Rails.application.config.assets.precompile += %w( manage/tours.js )

Rails.application.config.assets.precompile += %w( manage/offers.css )
Rails.application.config.assets.precompile += %w( manage/offers.js )

Rails.application.config.assets.precompile += %w( users/tours.css )
Rails.application.config.assets.precompile += %w( users/tours.js )

Rails.application.config.assets.precompile += %w( users/offers.css )
Rails.application.config.assets.precompile += %w( users/offers.js )

Rails.application.config.assets.precompile += %w( accommodations.js )

Rails.application.config.assets.precompile += %w( contests.css )
Rails.application.config.assets.precompile += %w( contests.js)

Rails.application.config.assets.precompile += %w( events.js )
Rails.application.config.assets.precompile += %w( events.css )

Rails.application.config.assets.precompile += %w( jquery.tagit.css )
Rails.application.config.assets.precompile += %w( tag-it.css )
