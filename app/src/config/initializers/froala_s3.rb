AWS_CONFIG = {
  'access_key_id' => ENV['AMAZON_ACCESS_KEY_ID'],
  'secret_access_key' => ENV['AMAZON_SECRET_ACCESS_KEY'],
  'bucket' => ENV['AMAZON_BUCKET'],
  'acl' => 'public-read',
  'key_start' => 'uploads/froala/'
}
