class AddSnowForecastIdToResorts < ActiveRecord::Migration
  def change
    add_column :resorts, :snow_forecast_id, :string
  end
end
