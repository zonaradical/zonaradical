class CreateInstagramImages < ActiveRecord::Migration
  def change
    create_table :instagram_images do |t|
      t.string :instagram_id
      t.string :hashtag
      t.json :data

      t.timestamps
    end

    add_index :instagram_images, :instagram_id
  end
end
