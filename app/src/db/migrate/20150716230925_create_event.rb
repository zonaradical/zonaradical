class CreateEvent < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :slug
      t.text   :short_description
      t.text   :description
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end

    add_index :events, :title
    add_index :events, :start_date
    add_index :events, :end_date
  end
end
