class CreateEventCountryAssignments < ActiveRecord::Migration
  def change
    create_table :event_country_assignments do |t|
      t.references :event, index: true
      t.references :resort_category, index: true
    end
  end
end
