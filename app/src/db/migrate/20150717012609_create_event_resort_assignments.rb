class CreateEventResortAssignments < ActiveRecord::Migration
  def change
    create_table :event_resort_assignments do |t|
      t.references :event, index: true
      t.references :resort, index: true
    end
  end
end
