class AddPublishedToInstagramImages < ActiveRecord::Migration
  def change
    add_column :instagram_images, :published, :boolean, :default => true
  end
end
