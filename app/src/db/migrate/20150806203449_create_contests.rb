class CreateContests < ActiveRecord::Migration
  def change
    create_table :contests do |t|
      t.string :title
      t.string :hashtag
      t.text :description
      t.string :image
      t.string :slug
      t.date :start_date
      t.integer :start_date_d
      t.integer :start_date_m
      t.integer :start_date_y

      t.timestamps
    end
  end
end
