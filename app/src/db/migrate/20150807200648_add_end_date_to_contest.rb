class AddEndDateToContest < ActiveRecord::Migration
  def change
    add_column :contests, :end_date, :date
    add_column :contests, :end_date_y, :integer
    add_column :contests, :end_date_m, :integer
    add_column :contests, :end_date_d, :integer
  end
end
