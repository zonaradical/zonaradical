class AddFbLikesToInstagramImages < ActiveRecord::Migration
  def change
    add_column :instagram_images, :fb_likes, :integer
  end
end
