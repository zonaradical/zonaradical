class AddNoPaginationToContest < ActiveRecord::Migration
  def change
    add_column :contests, :no_pagination, :boolean, default: true
  end
end
