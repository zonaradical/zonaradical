class AddTypeToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :active_from, :date
    add_column :offers, :active_till, :date
    add_column :offers, :active_from_d, :integer
    add_column :offers, :active_from_m, :integer
    add_column :offers, :active_from_y, :integer
    add_column :offers, :active_till_d, :integer
    add_column :offers, :active_till_m, :integer
    add_column :offers, :active_till_y, :integer
    add_column :offers, :kind, :integer
  end
end
