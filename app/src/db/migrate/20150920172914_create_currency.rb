class CreateCurrency < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :name
      t.string :code, lenght: 3
      t.string :symbol
    end

    change_table :offers do |t|
      t.belongs_to :currency, default: 1
    end
  end
end
