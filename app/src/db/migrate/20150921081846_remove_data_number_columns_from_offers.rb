class RemoveDataNumberColumnsFromOffers < ActiveRecord::Migration
  def change
    remove_column :offers, :active_from_d, :integer
    remove_column :offers, :active_from_m, :integer
    remove_column :offers, :active_from_y, :integer
    remove_column :offers, :active_till_d, :integer
    remove_column :offers, :active_till_m, :integer
    remove_column :offers, :active_till_y, :integer
  end
end
