class AddDescriptionTypeToGalleryImages < ActiveRecord::Migration
  def change
    add_column :gallery_images, :description_type, :integer, default: 0
  end
end
