class AddDiscourseTopicIdToTips < ActiveRecord::Migration
  def change
    add_column :tips, :discourse_topic_id, :integer
  end
end
