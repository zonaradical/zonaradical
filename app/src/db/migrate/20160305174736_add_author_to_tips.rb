class AddAuthorToTips < ActiveRecord::Migration
  def change
    add_reference :tips, :author, index: true
  end
end
