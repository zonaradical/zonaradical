class AddDiscourseCategoryToResortCategories < ActiveRecord::Migration
  def change
    add_column :resort_categories, :discourse_category, :integer
  end
end
