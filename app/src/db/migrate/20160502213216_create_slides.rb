class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.string :title, limit: 255

      t.timestamps
    end
  end
end
