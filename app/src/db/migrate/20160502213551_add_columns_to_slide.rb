class AddColumnsToSlide < ActiveRecord::Migration
  def change
    add_column :slides, :description, :string, limit: 255
    add_column :slides, :image, :string, limit: 255
    add_column :slides, :status, :boolean
  end
end
