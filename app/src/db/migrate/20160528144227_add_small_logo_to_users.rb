class AddSmallLogoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :small_logo, :string
  end
end
