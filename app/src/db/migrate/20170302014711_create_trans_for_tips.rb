class CreateTransForTips < ActiveRecord::Migration
  def self.up
    Tip.create_translation_table!({
                                     :title => :string,
                                     :short_description => :text,
                                     :level1_description => :text,
                                     :level2_description => :text,
                                     :level2_description => :text
                                   }, {
                                     :migrate_data => true,
                                     :remove_source_columns => true
                                   })
  end

  def self.down
    Tip.drop_translation_table! :migrate_data => true
  end
end
