class AddOriginalDateToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :orig_date, :datetime
  end
end
