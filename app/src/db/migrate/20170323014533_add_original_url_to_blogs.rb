class AddOriginalUrlToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :orig_url, :string
  end
end
