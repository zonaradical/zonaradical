# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170323014533) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "pg_trgm"

  create_table "accommodations", force: true do |t|
    t.string    "name"
    t.timestamp "created_at",  precision: 6
    t.timestamp "updated_at",  precision: 6
    t.text      "description"
  end

  create_table "active_admin_comments", force: true do |t|
    t.string    "namespace"
    t.text      "body"
    t.string    "resource_id",                 null: false
    t.string    "resource_type",               null: false
    t.integer   "author_id"
    t.string    "author_type"
    t.timestamp "created_at",    precision: 6
    t.timestamp "updated_at",    precision: 6
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "articles", force: true do |t|
    t.text      "body"
    t.text      "title"
    t.integer   "user_id"
    t.timestamp "created_at", precision: 6
    t.timestamp "updated_at", precision: 6
  end

  add_index "articles", ["user_id", "created_at"], name: "index_articles_on_user_id_and_created_at", using: :btree

  create_table "blogs", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
    t.integer  "user_id"
    t.datetime "orig_date"
    t.string   "orig_url"
  end

  create_table "breeze_categories", force: true do |t|
    t.string    "name"
    t.text      "description"
    t.string    "ancestry"
    t.string    "index"
    t.integer   "ancestry_depth",               default: 0
    t.timestamp "created_at",     precision: 6
    t.timestamp "updated_at",     precision: 6
    t.string    "image"
  end

  create_table "breezes", force: true do |t|
    t.string    "title"
    t.text      "body"
    t.string    "url"
    t.string    "image"
    t.integer   "breeze_category_id"
    t.timestamp "created_at",         precision: 6
    t.timestamp "updated_at",         precision: 6
  end

  create_table "contests", force: true do |t|
    t.string    "title"
    t.string    "hashtag"
    t.text      "description"
    t.string    "image"
    t.string    "slug"
    t.date      "start_date"
    t.integer   "start_date_d"
    t.integer   "start_date_m"
    t.integer   "start_date_y"
    t.timestamp "created_at",    precision: 6
    t.timestamp "updated_at",    precision: 6
    t.text      "rules"
    t.date      "end_date"
    t.integer   "end_date_y"
    t.integer   "end_date_m"
    t.integer   "end_date_d"
    t.boolean   "no_pagination",               default: true
  end

  create_table "currencies", force: true do |t|
    t.string "name"
    t.string "code"
    t.string "symbol"
  end

  create_table "event_country_assignments", force: true do |t|
    t.integer "event_id"
    t.integer "resort_category_id"
  end

  add_index "event_country_assignments", ["event_id"], name: "index_event_country_assignments_on_event_id", using: :btree
  add_index "event_country_assignments", ["resort_category_id"], name: "index_event_country_assignments_on_resort_category_id", using: :btree

  create_table "event_resort_assignments", force: true do |t|
    t.integer "event_id"
    t.integer "resort_id"
  end

  add_index "event_resort_assignments", ["event_id"], name: "index_event_resort_assignments_on_event_id", using: :btree
  add_index "event_resort_assignments", ["resort_id"], name: "index_event_resort_assignments_on_resort_id", using: :btree

  create_table "events", force: true do |t|
    t.string    "title"
    t.string    "slug"
    t.text      "short_description"
    t.text      "description"
    t.timestamp "start_date",        precision: 6
    t.timestamp "end_date",          precision: 6
    t.timestamp "created_at",        precision: 6
    t.timestamp "updated_at",        precision: 6
    t.string    "image"
  end

  add_index "events", ["end_date"], name: "index_events_on_end_date", using: :btree
  add_index "events", ["start_date"], name: "index_events_on_start_date", using: :btree
  add_index "events", ["title"], name: "index_events_on_title", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string    "slug",                                    null: false
    t.integer   "sluggable_id",                            null: false
    t.string    "sluggable_type", limit: 50
    t.string    "scope"
    t.timestamp "created_at",                precision: 6
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "gallery_images", force: true do |t|
    t.integer   "resort_id"
    t.string    "image"
    t.text      "description"
    t.integer   "gallerable_id"
    t.string    "gallerable_type"
    t.timestamp "created_at",       precision: 6
    t.timestamp "updated_at",       precision: 6
    t.string    "name"
    t.string    "url"
    t.integer   "order"
    t.integer   "description_type",               default: 0
  end

  create_table "identities", force: true do |t|
    t.integer   "user_id"
    t.string    "provider"
    t.string    "uid"
    t.timestamp "created_at", precision: 6
    t.timestamp "updated_at", precision: 6
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "image_galleries", force: true do |t|
    t.string    "title"
    t.text      "description"
    t.integer   "image_gallerable_id"
    t.timestamp "created_at",            precision: 6
    t.timestamp "updated_at",            precision: 6
    t.string    "image_gallerable_type"
  end

  add_index "image_galleries", ["image_gallerable_id", "image_gallerable_type"], name: "index_image_gallerable_polymorphic", using: :btree

  create_table "instagram_images", force: true do |t|
    t.string    "instagram_id"
    t.string    "hashtag"
    t.json      "data"
    t.timestamp "created_at",   precision: 6
    t.timestamp "updated_at",   precision: 6
    t.boolean   "published",                  default: true
    t.integer   "fb_likes"
  end

  add_index "instagram_images", ["instagram_id"], name: "index_instagram_images_on_instagram_id", using: :btree

  create_table "mailboxer_conversation_opt_outs", force: true do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: true do |t|
    t.string    "subject",                  default: ""
    t.timestamp "created_at", precision: 6,              null: false
    t.timestamp "updated_at", precision: 6,              null: false
  end

  create_table "mailboxer_notifications", force: true do |t|
    t.string    "type"
    t.text      "body"
    t.string    "subject",                            default: ""
    t.integer   "sender_id"
    t.string    "sender_type"
    t.integer   "conversation_id"
    t.boolean   "draft",                              default: false
    t.string    "notification_code"
    t.integer   "notified_object_id"
    t.string    "notified_object_type"
    t.string    "attachment"
    t.timestamp "updated_at",           precision: 6,                 null: false
    t.timestamp "created_at",           precision: 6,                 null: false
    t.boolean   "global",                             default: false
    t.timestamp "expires",              precision: 6
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: true do |t|
    t.integer   "receiver_id"
    t.string    "receiver_type"
    t.integer   "notification_id",                                          null: false
    t.boolean   "is_read",                                  default: false
    t.boolean   "trashed",                                  default: false
    t.boolean   "deleted",                                  default: false
    t.string    "mailbox_type",    limit: 25
    t.timestamp "created_at",                 precision: 6,                 null: false
    t.timestamp "updated_at",                 precision: 6,                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "media_image_categories", force: true do |t|
    t.string    "name"
    t.text      "description"
    t.string    "ancestry"
    t.string    "index"
    t.integer   "ancestry_depth",               default: 0
    t.timestamp "created_at",     precision: 6
    t.timestamp "updated_at",     precision: 6
    t.string    "image"
  end

  create_table "offer_country_assignments", force: true do |t|
    t.integer "offer_id"
    t.integer "resort_category_id"
  end

  add_index "offer_country_assignments", ["offer_id"], name: "index_offer_country_assignments_on_offer_id", using: :btree
  add_index "offer_country_assignments", ["resort_category_id"], name: "index_offer_country_assignments_on_resort_category_id", using: :btree

  create_table "offer_resort_assignments", force: true do |t|
    t.integer "offer_id"
    t.integer "resort_id"
  end

  add_index "offer_resort_assignments", ["offer_id"], name: "index_offer_resort_assignments_on_offer_id", using: :btree
  add_index "offer_resort_assignments", ["resort_id"], name: "index_offer_resort_assignments_on_resort_id", using: :btree

  create_table "offer_user_assignments", force: true do |t|
    t.integer "offer_id"
    t.integer "user_id"
  end

  add_index "offer_user_assignments", ["offer_id"], name: "index_offer_user_assignments_on_offer_id", using: :btree
  add_index "offer_user_assignments", ["user_id"], name: "index_offer_user_assignments_on_user_id", using: :btree

  create_table "offer_user_participant_assignments", force: true do |t|
    t.integer "offer_id"
    t.integer "user_id"
    t.integer "status",   default: 0
  end

  add_index "offer_user_participant_assignments", ["offer_id"], name: "index_offer_user_participant_assignments_on_offer_id", using: :btree
  add_index "offer_user_participant_assignments", ["user_id"], name: "index_offer_user_participant_assignments_on_user_id", using: :btree

  create_table "offers", force: true do |t|
    t.integer   "tour_style_id"
    t.integer   "accommodation_id"
    t.string    "title"
    t.text      "description"
    t.string    "whats_included"
    t.integer   "duration"
    t.integer   "check_in_d"
    t.integer   "check_in_m"
    t.integer   "check_in_y"
    t.integer   "switch_off_d"
    t.integer   "switch_off_m"
    t.integer   "switch_off_y"
    t.date      "check_in"
    t.date      "switch_off"
    t.string    "image"
    t.decimal   "price",            precision: 5, scale: 0
    t.boolean   "published",                                default: true
    t.timestamp "created_at",       precision: 6
    t.timestamp "updated_at",       precision: 6
    t.string    "hotel_name"
    t.boolean   "air_included"
    t.string    "slug"
    t.date      "active_from"
    t.date      "active_till"
    t.integer   "kind"
    t.integer   "currency_id",                              default: 1
  end

  add_index "offers", ["accommodation_id"], name: "index_offers_on_accommodation_id", using: :btree
  add_index "offers", ["tour_style_id"], name: "index_offers_on_tour_style_id", using: :btree

  create_table "remarks", force: true do |t|
    t.integer   "user_id"
    t.string    "source_url"
    t.text      "content"
    t.timestamp "created_at", precision: 6
    t.timestamp "updated_at", precision: 6
  end

  create_table "resort_categories", force: true do |t|
    t.string  "name"
    t.text    "description"
    t.string  "ancestry"
    t.string  "index"
    t.integer "ancestry_depth",     default: 0
    t.string  "image"
    t.string  "slug"
    t.integer "discourse_category"
  end

  create_table "resorts", force: true do |t|
    t.string    "name"
    t.string    "image"
    t.string    "web"
    t.string    "fb"
    t.integer   "resort_category_id"
    t.integer   "index"
    t.text      "level1_description"
    t.text      "level2_description"
    t.text      "level3_description"
    t.string    "airport"
    t.integer   "altitude_top"
    t.integer   "altitude_bottom"
    t.integer   "drop"
    t.integer   "terrain"
    t.integer   "lifts"
    t.string    "slopes"
    t.timestamp "created_at",         precision: 6
    t.timestamp "updated_at",         precision: 6
    t.string    "map_url"
    t.string    "slug"
    t.string    "snow_forecast_id"
  end

  create_table "role_assignments", force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  create_table "roles", force: true do |t|
    t.string "name"
  end

  create_table "slides", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
    t.string   "image"
    t.boolean  "status"
    t.string   "link"
  end

  create_table "taggings", force: true do |t|
    t.integer   "tag_id"
    t.integer   "taggable_id"
    t.string    "taggable_type"
    t.integer   "tagger_id"
    t.string    "tagger_type"
    t.string    "context",       limit: 128
    t.timestamp "created_at",                precision: 6
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "tip_categories", force: true do |t|
    t.string    "name"
    t.text      "description"
    t.string    "index"
    t.integer   "ancestry_depth",               default: 0
    t.timestamp "created_at",     precision: 6
    t.timestamp "updated_at",     precision: 6
    t.string    "image"
    t.string    "slug"
  end

  create_table "tip_translations", force: true do |t|
    t.integer  "tip_id",             null: false
    t.string   "locale",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.text     "short_description"
    t.text     "level1_description"
    t.text     "level2_description"
  end

  add_index "tip_translations", ["locale"], name: "index_tip_translations_on_locale", using: :btree
  add_index "tip_translations", ["tip_id"], name: "index_tip_translations_on_tip_id", using: :btree

  create_table "tips", force: true do |t|
    t.integer   "tip_category_id"
    t.string    "image"
    t.timestamp "created_at",         precision: 6
    t.timestamp "updated_at",         precision: 6
    t.text      "level3_description"
    t.string    "slug"
    t.integer   "discourse_topic_id"
    t.integer   "author_id"
    t.integer   "comments_count",                   default: 0
  end

  add_index "tips", ["author_id"], name: "index_tips_on_author_id", using: :btree

  create_table "tour_country_assignments", force: true do |t|
    t.integer "tour_id"
    t.integer "resort_category_id"
  end

  add_index "tour_country_assignments", ["resort_category_id"], name: "index_tour_country_assignments_on_resort_category_id", using: :btree
  add_index "tour_country_assignments", ["tour_id"], name: "index_tour_country_assignments_on_tour_id", using: :btree

  create_table "tour_resort_assignments", force: true do |t|
    t.integer "tour_id"
    t.integer "resort_id"
  end

  add_index "tour_resort_assignments", ["resort_id"], name: "index_tour_resort_assignments_on_resort_id", using: :btree
  add_index "tour_resort_assignments", ["tour_id"], name: "index_tour_resort_assignments_on_tour_id", using: :btree

  create_table "tour_styles", force: true do |t|
    t.string    "name"
    t.string    "description"
    t.timestamp "created_at",  precision: 6
    t.timestamp "updated_at",  precision: 6
  end

  create_table "tour_user_assignments", force: true do |t|
    t.integer "tour_id"
    t.integer "user_id"
  end

  add_index "tour_user_assignments", ["tour_id"], name: "index_tour_user_assignments_on_tour_id", using: :btree
  add_index "tour_user_assignments", ["user_id"], name: "index_tour_user_assignments_on_user_id", using: :btree

  create_table "tour_user_participant_assignments", force: true do |t|
    t.integer "tour_id"
    t.integer "user_id"
    t.integer "status",  default: 0
  end

  add_index "tour_user_participant_assignments", ["tour_id"], name: "index_tour_user_participant_assignments_on_tour_id", using: :btree
  add_index "tour_user_participant_assignments", ["user_id"], name: "index_tour_user_participant_assignments_on_user_id", using: :btree

  create_table "tours", force: true do |t|
    t.integer   "tour_style_id"
    t.integer   "accommodation_id"
    t.string    "title"
    t.text      "description"
    t.string    "whats_included"
    t.integer   "duration"
    t.string    "image"
    t.decimal   "price",              precision: 5, scale: 0
    t.boolean   "published",                                  default: true
    t.timestamp "created_at",         precision: 6
    t.timestamp "updated_at",         precision: 6
    t.integer   "check_in_d"
    t.integer   "check_in_m"
    t.integer   "check_in_y"
    t.integer   "switch_off_d"
    t.integer   "switch_off_m"
    t.integer   "switch_off_y"
    t.date      "check_in"
    t.date      "switch_off"
    t.integer   "discourse_topic_id"
    t.string    "slug"
  end

  add_index "tours", ["accommodation_id"], name: "index_tours_on_accommodation_id", using: :btree
  add_index "tours", ["tour_style_id"], name: "index_tours_on_tour_style_id", using: :btree

  create_table "users", force: true do |t|
    t.string    "email",                                default: "",               null: false
    t.string    "encrypted_password",                   default: "",               null: false
    t.string    "reset_password_token"
    t.timestamp "reset_password_sent_at", precision: 6
    t.timestamp "remember_created_at",    precision: 6
    t.integer   "sign_in_count",                        default: 0,                null: false
    t.timestamp "current_sign_in_at",     precision: 6
    t.timestamp "last_sign_in_at",        precision: 6
    t.string    "current_sign_in_ip"
    t.string    "last_sign_in_ip"
    t.string    "confirmation_token"
    t.timestamp "confirmed_at",           precision: 6
    t.timestamp "confirmation_sent_at",   precision: 6
    t.string    "unconfirmed_email"
    t.timestamp "created_at",             precision: 6
    t.timestamp "updated_at",             precision: 6
    t.string    "name"
    t.string    "forem_state",                          default: "pending_review"
    t.boolean   "forem_auto_subscribe",                 default: false
    t.timestamp "last_seen_at",           precision: 6
    t.string    "avatar"
    t.string    "surname"
    t.string    "login"
    t.string    "sex"
    t.date      "birthday"
    t.string    "country"
    t.string    "city"
    t.string    "web"
    t.string    "fb"
    t.text      "bio"
    t.string    "fb_avatar"
    t.string    "image"
    t.string    "telephone"
    t.string    "slug"
    t.string    "small_logo"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "video_categories", force: true do |t|
    t.string    "name"
    t.text      "description"
    t.string    "ancestry"
    t.string    "index"
    t.integer   "ancestry_depth",               default: 0
    t.timestamp "created_at",     precision: 6
    t.timestamp "updated_at",     precision: 6
    t.string    "image"
    t.string    "slug"
  end

  create_table "videos", force: true do |t|
    t.string    "title"
    t.text      "description"
    t.integer   "source_cd"
    t.integer   "video_category_id"
    t.string    "source_link"
    t.timestamp "created_at",        precision: 6
    t.timestamp "updated_at",        precision: 6
    t.string    "slug"
  end

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", name: "mb_opt_outs_on_conversations_id", column: "conversation_id"

  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", name: "notifications_on_conversation_id", column: "conversation_id"

  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", name: "receipts_on_notification_id", column: "notification_id"

end
