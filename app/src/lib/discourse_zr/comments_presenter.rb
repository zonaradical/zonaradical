module DiscourseZr
  class CommentsPresenter
    attr_reader :all_comments

    def initialize(all_comments)
      @all_comments = all_comments
    end

    def main_comments
      @all_comments.select { |c| c['reply_to_post_number'] == nil }
    end

    def any?
      @all_comments.any?
    end
  end
end
