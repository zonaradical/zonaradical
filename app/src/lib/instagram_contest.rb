module InstagramContest

  INSTAGRAM_CLIENT_ID = Rails.application.secrets.instagram_client_id

  def self.add_media_by_hashtag (hashtag,count=nil,max_tag_id=nil)
    max_tag_id ||= '0'
    count ||= '100'
    url = "https://api.instagram.com/v1/tags/#{hashtag}/media/recent?client_id=#{INSTAGRAM_CLIENT_ID}&count=#{count}&max_tag_id=#{max_tag_id}"
    while url
      response = Net::HTTP.get(URI.parse(url))
      response_json = JSON::parse(response)
      for img in response_json['data']
        InstagramImage.find_or_create_by(:instagram_id => img['id']) do |instagram_image|
          instagram_image.data = img
          instagram_image.hashtag = hashtag
        end
      end
      url = response_json['pagination']['next_url']
    end
  end

  def self.sync_fb_likes_by_hashtag(hashtag)
    contest = Contest.find_by_hashtag hashtag
    images = InstagramImage.where(hashtag:hashtag).all
    unless images.nil?
      for image in images
        url = Rails.application.routes.url_helpers.contest_image_url({host: 'http://zonaradical.com.br', title: contest.slug, image_id: image.id})
        response = HTTParty.get("https://api.facebook.com/method/fql.query?query=select%20total_count,like_count,comment_count,share_count,click_count%20from%20link_stat%20where%20url=%27#{url}%27&format=json")
        fb_likes = JSON.parse(response.body)[0]["total_count"]
        image.fb_likes = fb_likes unless fb_likes.nil?
        image.save
      end
    end
  end
end
