namespace :contests do
  require 'net/http'
  require 'json'
  require 'instagram_contest'

  desc 'Search instagram images by hashtag'
  task :sync, [:hashtag,:count,:max_tag_id] => :environment do |t,args|
    begin
      args.with_defaults(hashtag: 'MaisEstiloZR', count: 100, max_tag_id: 0)
      InstagramContest::add_media_by_hashtag args[:hashtag], args[:count], args[:max_tag_id]
    end
  end

  desc 'Sync FB likes - total count'
  task :fb_sync, [:hashtag] => :environment do |t,args|
    begin
      args.with_defaults(hashtag: 'MaisEstiloZR')
      InstagramContest::sync_fb_likes_by_hashtag args[:hashtag]
    end
  end
end
