require 'highline/import'

namespace :db do
  desc 'Merge tags'
  task merge_tags: :environment do
    merged_name = ask('New name for merged tags: ')
    tags_to_be_merged = ask('list of tags to be merged, e.g: tag1,tag2 (without spaces): ').split(',')

    if merged_name.empty? || tags_to_be_merged.empty?
      puts 'Error: fill required fields: merged_name, tags_to_be_merged'
    else
      ActiveRecord::Base.transaction do
        merged_tag = Tag.find_or_create_by(name: merged_name)

        Tag.where(name: tags_to_be_merged).each do |tag|
          puts ">Start merging: #{tag.name}"

          tag.taggings.each do |tagging|
            resource = tagging.taggable

            puts ">Start merging resource: #{tagging.taggable_type} #{tagging.taggable_id}"

            puts "Old tag list: #{resource.tag_list}"
            resource.tag_list -= tags_to_be_merged
            resource.tag_list << merged_name
            resource.save!
            puts "New tag list: #{resource.tag_list}"

            puts "Finish merging resource: #{tagging.taggable_type} #{tagging.taggable_id}"
          end

          puts "Finish merging: #{tag.name}"
          puts '-'
        end
      end
    end
  end
end
