require 'highline/import'


namespace :uploader do
  desc 'Recreate uploader for specified resource(modelName)'
  task create: :environment do
    model = ask 'Please specify model name (leave empty if you like process all models): '
    if !model.blank?
      m = model.constantize
      m.uploaders.each_key do |key|
        m.all.each do |obj|
          say "Process #{model} with id = " + obj.id.to_s
          obj.send(key).recreate_versions! if obj.send("#{key}?")
          # todo if we will change uploaded file name use
          # https://github.com/carrierwaveuploader/carrierwave/wiki/How-to%3A-Create-random-and-unique-filenames-for-all-versioned-files
        end
      end
    else
      ActiveRecord::Base.connection.tables.each do |model|
        say "Processing #{model}"
        begin
          m = model.capitalize.singularize.camelize.constantize
          if m.respond_to? 'uploaders'
            m.uploaders.each_key do |key|
              m.all.each do |obj|
                say "Process #{model} with id = " + obj.id.to_s
                obj.send(key).recreate_versions! if obj.send("#{key}?")
              end
            end
          end
        rescue NameError
          say "#{model} can't be constantized"
        end
      end
    end
  end
end
