namespace :user do

  desc 'Create confirmed user, without sending confirmation email'
  task create: :environment do
    begin
      require 'highline/import'
      email = ask('Email: ')
      existing_user = User.find_by_email email
      if existing_user
        say 'User already exist'
      else
        name = ask('Name: ')
        surname = ask('Surname: ')
        begin
          password = ask("Password:  ") {|q| q.echo = false}
          password_confirmation = ask("Repeat password:  ") {|q| q.echo = false}
        end while password != password_confirmation

        # save/update user account
        saved = User.create!({
                               name: name,email: email,surname: surname,
                               password: password,password_confirmation: password_confirmation,
                               confirmed_at: Time.now
                             })
        if !saved
          puts User.errors.full_messages.join("\n")
          next
        end
      end
    end while !saved
  end

  desc 'Confirm user by email'
  task confirm: :environment do
    require 'highline/import'
    begin
      email = ask('User email: ')
      existing_user = User.find_by_email email
      if existing_user
        existing_user.confirm!
        existing_user.save!
        say 'User ' + email + ' has been confirmed!'
      else
        say 'User doesn\'t exist'
      end
    end
  end

  desc 'Change user email without confirmation'
  task change_email: :environment do
    require 'highline/import'
    begin
      old_email = ask('Old email: ')
      existing_user = User.find_by_email old_email
      if existing_user
        new_email = ask('New email: ')
        existing_user.skip_reconfirmation!
        existing_user.email=new_email
        existing_user.save!
          say 'User ' + old_email + ' has changed email to ' + existing_user.email
      else
        say 'User doesn\'t exist'
      end
    end
  end

  desc 'Add admin role to user by email'
  task admin: :environment do
    require 'highline/import'
    begin
      email = ask('User email: ')
      existing_user = User.find_by_email email
      if existing_user
        existing_user.roles << Role.find_by_name(:admin)
        existing_user.save!
        say 'Admin role has been assigned to ' + email + '!'
      else
        say 'User doesn\'t exist'
      end
    end
  end


end
