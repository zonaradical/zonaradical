require 'rails_helper'

RSpec.describe "Resorts", :type => :request, broken: true   do
  subject { page }

  describe 'GET /resorts' do
    before { visit resorts_path }
    describe 'with pagination' do
      it do
        should have_text ('Ski Resorts Hemisfério Norte')
        should have_text ('Argentina (8)')
        should have_text ('Austria (2)')
        should have_text ('Finlandia (2)')
        should have_text ('Italia (2)')
        should have_text ('Ski Resorts Hemisfério Sul')
      end
    end
  end
end
