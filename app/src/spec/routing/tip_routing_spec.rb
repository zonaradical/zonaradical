require "rails_helper"

RSpec.describe ResortsController, :type => :routing do
  describe "Tip" do

    it "Dicas routes to #index" do
      expect(:get => "/dicas-de-snowboard").to route_to("tips#index")
    end

    it "Dicas route to dicas id" do
      expect(:get => "/dicas-de-snowboard/category_id/id").to route_to({"controller"=>"tips", "action"=>"show", "category_id"=>"category_id", "id"=>"id"})
    end
  end
end
