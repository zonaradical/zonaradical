require 'vcr'

VCR.configure do |c|
  # Uncomment to save new episodes
  #c.default_cassette_options = { :record => :new_episodes }

  c.cassette_library_dir = 'spec/vcr_cassettes'
  c.hook_into :webmock
end
